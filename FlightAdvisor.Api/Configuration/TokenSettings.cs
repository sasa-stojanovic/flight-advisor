﻿namespace FlightAdvisor.Api.Configuration
{
    public class TokenSettings
    {
        public string Key { get; set; }
        public string Issuer { get; set; }
    }
}
