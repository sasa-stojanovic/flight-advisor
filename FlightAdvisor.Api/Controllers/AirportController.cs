﻿using FlightAdvisor.Api.Lib.ServiceInterfaces;
using FlightAdvisor.Api.Models;
using FlightAdvisor.Common.Constants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace FlightAdvisor.Api.Controllers
{
    public class AirportController : ControllerBase
    {
        private readonly IAirportService _airportService;
        private readonly ILogger<TravelController> _logger;

        public AirportController(IAirportService airportService, ILogger<TravelController> logger)
        {
            _airportService = airportService;
            _logger = logger;
        }

        [HttpPost]
        [Authorize(Roles = RoleConstants.Admin)]
        public async Task<ApiResultModel> Import([FromForm] IFormFile file)
        {
            try
            {
                await _airportService.ImportAirports(file);
                return new ApiResultModel();

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return new ApiResultModel(ex);
            }
        }
    }
}
