﻿using FlightAdvisor.Api.Lib.Enums;
using FlightAdvisor.Api.Lib.Models;
using FlightAdvisor.Api.Lib.ServiceInterfaces;
using FlightAdvisor.Api.Models;
using FlightAdvisor.Common.Constants;
using FlightAdvisor.Common.Exceptions.General;
using FlightAdvisor.Common.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace FlightAdvisor.Api.Controllers
{
    public class CommentController : ControllerBase
    {
        private readonly ICommentService _commentService;
        private readonly ILogger<TravelController> _logger;

        public CommentController(ICommentService commentService, ILogger<TravelController> logger)
        {
            _commentService = commentService;
            _logger = logger;
        }

        [HttpPost]
        [Authorize(Roles = RoleConstants.User)]
        public async Task<ApiResultModel<int>> Create([FromBody] CommentModel comment)
        {
            try
            {
                if (!ModelState.IsValid) throw new InvalidModelDataException(ModelState);

                return new ApiResultModel<int>(await _commentService.Create(comment, User));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return new ApiResultModel<int>(ex);
            }
        }

        [HttpPut]
        [Authorize(Roles = RoleConstants.User)]
        public async Task<ApiResultModel> Edit([FromBody] CommentModel comment)
        {
            try
            {
                if (!ModelState.IsValid) throw new InvalidModelDataException(ModelState);

                await _commentService.Edit(comment, User);

                return new ApiResultModel();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return new ApiResultModel(ex);
            }
        }

        [HttpDelete]
        [Authorize(Roles = RoleConstants.User)]
        public async Task<ApiResultModel> Delete([FromBody] DeleteModel delete)
        {
            try
            {
                if (!ModelState.IsValid) throw new InvalidModelDataException(ModelState);

                await _commentService.Delete(delete, User);

                return new ApiResultModel();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return new ApiResultModel(ex);
            }
        }
    }
}
