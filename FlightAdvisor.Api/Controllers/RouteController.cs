﻿using FlightAdvisor.Api.Models;
using FlightAdvisor.Api.Lib.ServiceInterfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using FlightAdvisor.Common.Constants;

namespace FlightAdvisor.Api.Controllers
{
    public class RouteController : ControllerBase
    {
        private readonly IRouteService _routeService;
        private readonly ILogger<TravelController> _logger;

        public RouteController(IRouteService routeService, ILogger<TravelController> logger)
        {
            _routeService = routeService;
            _logger = logger;
        }

        [HttpPost]
        [Authorize(Roles = RoleConstants.Admin)]
        public async Task<ApiResultModel> Import([FromForm] IFormFile file)
        {
            try
            {
                await _routeService.ImportRoutes(file);
                return new ApiResultModel();

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return new ApiResultModel(ex);
            }
        }
    }
}
