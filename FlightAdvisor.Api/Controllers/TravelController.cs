﻿using FlightAdvisor.Api.Models;
using FlightAdvisor.Api.Lib.ServiceInterfaces;
using FlightAdvisor.Common.Exceptions.General;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FlightAdvisor.Api.Lib.Models;
using FlightAdvisor.Common.Constants;

namespace FlightAdvisor.Api.Controllers
{
    public class TravelController : ControllerBase
    {
        private readonly ITravelService _travelService;
        private readonly ILogger<TravelController> _logger;

        public TravelController(ITravelService travelService, ILogger<TravelController> logger)
        {
            _travelService = travelService;
            _logger = logger;
        }

        [HttpGet]
        [Authorize(Roles = RoleConstants.User)]
        public async Task<ApiResultModel<FlightRouteResultModel>> GetCheapestFlightRoute([FromQuery] FlightRouteModel flightRoute)
        {
            try
            {
                if (!ModelState.IsValid) throw new InvalidModelDataException(ModelState);

                return new ApiResultModel<FlightRouteResultModel>(await _travelService.GetCheapestFlightRoute(flightRoute));

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return new ApiResultModel<FlightRouteResultModel>(ex);
            }
        }

        [HttpGet]
        [Authorize(Roles = RoleConstants.User)]
        public async Task<ApiResultModel<FlightRouteResultModel>> GetOptimalFlightRoute([FromQuery] FlightRouteModel flightRoute)
        {
            try
            {
                if (!ModelState.IsValid) throw new InvalidModelDataException(ModelState);

                return new ApiResultModel<FlightRouteResultModel>(await _travelService.GetOptimalFlightRoute(flightRoute));

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return new ApiResultModel<FlightRouteResultModel>(ex);
            }
        }
    }
}
