﻿using FlightAdvisor.Api.Lib.Enums;
using FlightAdvisor.Api.Lib.Models;
using FlightAdvisor.Api.Lib.ServiceInterfaces;
using FlightAdvisor.Api.Models;
using FlightAdvisor.Common.Constants;
using FlightAdvisor.Common.Exceptions.General;
using FlightAdvisor.Common.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace FlightAdvisor.Api.Controllers
{
    public class CityController : ControllerBase
    {
        private readonly ICityService _cityService;
        private readonly ILogger<TravelController> _logger;

        public CityController(ICityService cityService, ILogger<TravelController> logger)
        {
            _cityService = cityService;
            _logger = logger;
        }

        [HttpPost]
        [Authorize(Roles = RoleConstants.Admin)]
        public async Task<ApiResultModel<int>> Create([FromBody] CityModel city)
        {
            try
            {
                if (!ModelState.IsValid) throw new InvalidModelDataException(ModelState);

                return new ApiResultModel<int>(await _cityService.Create(city));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return new ApiResultModel<int>(ex);
            }
        }

        [HttpGet]
        [Authorize(Roles = RoleConstants.User)]
        public async Task<ApiResultModel<ListResultModel<CityModel>>> List([FromQuery] ListCitiesModel<CitySortColumn> list)
        {
            try
            {
                if (!ModelState.IsValid) throw new InvalidModelDataException(ModelState);

                return new ApiResultModel<ListResultModel<CityModel>>(await _cityService.List(list));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return new ApiResultModel<ListResultModel<CityModel>>(ex);
            }
        }
    }
}
