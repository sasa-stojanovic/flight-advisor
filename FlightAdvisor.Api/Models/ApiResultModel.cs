﻿using System;
using FlightAdvisor.Common.Models;
using FlightAdvisor.Api.Helpers;

namespace FlightAdvisor.Api.Models
{
    public class ApiResultModel : ResultModel
    {
        public ApiResultModel() : base()
        {

        }

        public ApiResultModel(Exception e) : base(e)
        {
            ErrorType = e.GetErrorType();
        }
    }

    public class ApiResultModel<T> : ResultModel<T>
    {
        public ApiResultModel() : base()
        {

        }

        public ApiResultModel(Exception e) : base(e)
        {
            ErrorType = e.GetErrorType();
        }

        public ApiResultModel(T data) : base(data)
        {

        }
    }
}
