﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Text;
using System;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using System.Net.Mime;
using FlightAdvisor.Api.Constants;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using FlightAdvisor.Common.Exceptions.Auth;
using Newtonsoft.Json.Serialization;
using FlightAdvisor.Api.Models;
using FlightAdvisor.Api.Configuration;
using FlightAdvisor.Api.Lib.Extensions;

namespace FlightAdvisor.Api.Extensions
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddFlightAdvisorApiServices(this IServiceCollection services, IConfiguration configuration)
        {
            var apiSettingsSection = configuration.GetSection(ConfigConstants.ApiSettings);
            services.Configure<ApiSettings>(apiSettingsSection);
            var apiSettings = apiSettingsSection.Get<ApiSettings>();

            // ===== Add Jwt Authentication ========
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear(); // => remove default claims
            services
                .AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(cfg =>
                {
                    cfg.RequireHttpsMetadata = false;
                    cfg.SaveToken = true;
                    cfg.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidIssuer = apiSettings.Token.Issuer,
                        ValidAudience = apiSettings.Token.Issuer,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(apiSettings.Token.Key)),
                        ClockSkew = TimeSpan.Zero // remove delay of token when expire
                    };
                    cfg.Events = new JwtBearerEvents
                    {
                        OnChallenge = context =>
                        {
                            context.Response.ContentType = MediaTypeNames.Application.Json;
                            if (context.AuthenticateFailure != null && context.AuthenticateFailure.GetType() == typeof(SecurityTokenExpiredException))
                            {
                                context.Response.WriteAsync(JsonConvert.SerializeObject(new ApiResultModel(new AuthExpiredTokenException()), new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() })).Wait();
                            }
                            else
                            {
                                context.Response.WriteAsync(JsonConvert.SerializeObject(new ApiResultModel(new AuthInvalidTokenException()), new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() })).Wait();
                            }

                            context.HandleResponse();
                            return Task.CompletedTask;
                        }
                    };
                });

            services.AddFlightAdvisorApiLibServices(configuration);

            return services;
        }
    }
}
