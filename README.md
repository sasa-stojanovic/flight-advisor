# Flight Advisor

## Initial users

**Administrator:** username: admin, password: asdQWE123#

**Regular user:** username: user, password: asdQWE123#

## Notes

Postman collection provided in root for quick testing.

In appsettings.json for FlightAdvisor.Api (LibSettings -> Import -> ImportCities), option for importing all cities can be turned on.

Since there are 2 APIs, both should be run in order for whole system to work.

## Authentication

Login action to Auth API should be used to obtain access token, which should be used for further api calls.

## Flight route calculation

There are 2 endpoints for flight route calculation:
 - GetCheapestFlightRoute: calculates cheapest route
 - GetOptimalFlightRoute: heuristic method for calculating route (not always the cheapest one, but much faster)
