﻿using FlightAdvisor.Auth.Lib.Configuration;
using FlightAdvisor.Auth.Database.Contexts;
using FlightAdvisor.Auth.Database.Models;
using FlightAdvisor.Auth.Lib.ServiceInterfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Linq;
using FlightAdvisor.Common.Constants;

namespace FlightAdvisor.Auth.Lib.Services
{
    public class ConfigurationService : IConfigurationService
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly UserManager<IdentityAppUser> _userManager;
        private readonly RoleManager<IdentityAppRole> _roleManager;
        private readonly AuthSettings _authSettings;

        public ConfigurationService(ApplicationDbContext dbContext, UserManager<IdentityAppUser> userManager, RoleManager<IdentityAppRole> roleManager, IOptions<AuthSettings> authSettings)
        {
            _dbContext = dbContext;
            _userManager = userManager;
            _roleManager = roleManager;
            _authSettings = authSettings.Value;
        }

        public void EnsureDatabaseCreated()
        {
            _dbContext.Database.Migrate();
        }

        public void EnsureRolesCreated()
        {
            foreach (var roleName in new[] { RoleConstants.Admin, RoleConstants.User })
            {
                var roleExist = _roleManager.RoleExistsAsync(roleName);
                roleExist.Wait();
                if (!roleExist.Result)
                {
                    _roleManager.CreateAsync(new IdentityAppRole(roleName)).Wait();
                }
            }
        }

        public void EnsureInitialUsersCreated()
        {
            foreach (var initialUser in _authSettings.InitialUsers)
            {
                if (!_userManager.Users.Any(u => u.UserName == initialUser.UserName))
                {
                    var identityAppUser = new IdentityAppUser
                    {
                        UserName = initialUser.UserName,
                        FirstName = initialUser.FirstName,
                        LastName = initialUser.LastName,
                        Active = true,
                        SecurityStamp = Guid.NewGuid().ToString()
                    };
                    _userManager.CreateAsync(identityAppUser, initialUser.Password).Wait();
                    _userManager.AddToRolesAsync(identityAppUser, initialUser.Roles).Wait();
                }
            }
        }
    }
}
