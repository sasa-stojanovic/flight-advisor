﻿using FlightAdvisor.Auth.Database.Models;
using FlightAdvisor.Auth.Lib.ServiceInterfaces;
using FlightAdvisor.Common.Enums;
using FlightAdvisor.Common.Exceptions.Auth;
using FlightAdvisor.Common.Exceptions.General;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using FlightAdvisor.Common.Models;
using FlightAdvisor.Auth.Lib.Models;
using FlightAdvisor.Auth.Lib.Enums;
using FlightAdvisor.Auth.Lib.Helpers;
using FlightAdvisor.Common.Constants;

namespace FlightAdvisor.Auth.Lib.Services
{
    public class AccountService : IAccountService
    {
        private readonly UserManager<IdentityAppUser> _userManager;
        private readonly SignInManager<IdentityAppUser> _signInManager;
        private readonly ITokenService _tokenService;

        public AccountService(UserManager<IdentityAppUser> userManager, SignInManager<IdentityAppUser> signInManager, ITokenService tokenService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _tokenService = tokenService;
        }

        public async Task<TokenModel> Login(LoginModel login)
        {
            var loginResult = await _signInManager.PasswordSignInAsync(login.Username, login.Password, false, true);

            if (!loginResult.Succeeded) throw new AuthInvalidLoginAttemptException();

            IdentityAppUser identityAppUser = await _userManager.Users.Include(u => u.UserRoles).ThenInclude(ur => ur.Role).SingleOrDefaultAsync(u => u.UserName == login.Username);

            if (!identityAppUser.Active) throw new AuthInactiveUserException();

            return await _tokenService.GenerateTokensForUser(identityAppUser);
        }

        public async Task Logout(LogoutModel logout)
        {
            await _tokenService.DeleteRefreshToken(logout.RefreshToken);
        }

        public async Task<TokenModel> Register(UserModel user)
        {
            user.Roles = new string[] { RoleConstants.User };

            var identityAppUser = AccountHelper.BusinessToDb(user);

            await AccountHelper.ValidateUsername(_userManager, identityAppUser);

            await AccountHelper.ValidatePassword(_userManager, user.Password);

            var registerResult = await _userManager.CreateAsync(identityAppUser, user.Password);
            if (!registerResult.Succeeded)
            {
                throw new UnknownErrorException(registerResult.Errors);
            }

            var addRolesResult = await _userManager.AddToRolesAsync(identityAppUser, user.Roles);
            if (!addRolesResult.Succeeded)
            {
                throw new UnknownErrorException(addRolesResult.Errors);
            }

            identityAppUser = await _userManager.Users.Include(u => u.UserRoles).ThenInclude(ur => ur.Role).SingleOrDefaultAsync(u => u.Id == identityAppUser.Id);

            return await _tokenService.GenerateTokensForUser(identityAppUser);
        }

        public async Task<UserModel> Get(GetModel get)
        {
            var identityAppUser = await _userManager.Users.Include(u => u.UserRoles).ThenInclude(ur => ur.Role).FirstOrDefaultAsync(u => u.Id == get.Id);

            if (identityAppUser == null) throw new EntryNotFoundException("user", get.Id);

            return AccountHelper.DbToBusiness(identityAppUser);
        }

        public async Task<ListResultModel<UserModel>> List(ListModel<UserSortColumn> list)
        {
            ListResultModel<UserModel> result = new ListResultModel<UserModel>();

            IQueryable<IdentityAppUser> users = _userManager.Users.Include(u => u.UserRoles).ThenInclude(ur => ur.Role);

            result.Total = await users.CountAsync();
            result.Filtered = result.Total;

            if (list.Filter != null)
            {
                users = users.Where(u => EF.Functions.Like(u.FirstName, $"%{list.Filter}%") || EF.Functions.Like(u.LastName, $"%{list.Filter}%") || EF.Functions.Like(u.UserName, $"%{list.Filter}%"));
                result.Filtered = await users.CountAsync();
            }

            Expression<Func<IdentityAppUser, object>> orderBy = null;

            switch (list.SortColumn)
            {
                case UserSortColumn.FirstName:
                    orderBy = u => u.FirstName;
                    break;
                case UserSortColumn.LastName:
                    orderBy = u => u.LastName;
                    break;
                case UserSortColumn.Username:
                    orderBy = u => u.UserName;
                    break;
                case UserSortColumn.Active:
                    orderBy = u => u.Active;
                    break;
            }

            users = list.SortDirection == SortDirection.Asc ? users.OrderBy(orderBy) : users.OrderByDescending(orderBy);

            if (list.Page != null && list.PageSize != null)
            {
                users = users.Skip((list.Page.Value - 1) * list.PageSize.Value);
            }

            if (list.PageSize != null)
            {
                users = users.Take(list.PageSize.Value);
            }

            result.Data = await users.Select(u => AccountHelper.DbToBusiness(u)).ToListAsync();

            return result;
        }

        public async Task SetStatus(UserStatusModel userStatus)
        {
            var identityAppUser = await _userManager.Users.Include(u => u.UserRoles).ThenInclude(ur => ur.Role).FirstOrDefaultAsync(u => u.Id == userStatus.UserId);

            if (identityAppUser == null) throw new EntryNotFoundException("user", userStatus.UserId);

            identityAppUser.Active = userStatus.Active.Value;
            await _userManager.UpdateAsync(identityAppUser);
            if (!userStatus.Active.Value)
            {
                await _tokenService.DeleteRefreshTokensForUser(userStatus.UserId);
            }
        }

        public async Task Edit(UserModel user)
        {
            var identityAppUser = await _userManager.Users.Include(u => u.UserRoles).ThenInclude(ur => ur.Role).FirstOrDefaultAsync(u => u.Id == user.Id);

            if (identityAppUser == null) throw new EntryNotFoundException("user", user.Id);

            identityAppUser.FirstName = user.FirstName;
            identityAppUser.LastName = user.LastName;
            identityAppUser.UserName = user.Username;

            if (!string.IsNullOrEmpty(user.Username))
            {
                await AccountHelper.ValidateUsername(_userManager, identityAppUser);
            }

            if (!string.IsNullOrEmpty(user.Password))
            {
                await AccountHelper.ValidatePassword(_userManager, user.Password);

                await _userManager.RemovePasswordAsync(identityAppUser);
                await _userManager.AddPasswordAsync(identityAppUser, user.Password);
            }

            if (user.Roles != null)
            {
                var roles = identityAppUser.UserRoles.Select(r => r.Role.Name).ToArray();
                var rolesToRemove = roles.Where(t => !user.Roles.Contains(t)).ToArray();
                var rolesToAdd = user.Roles.Where(t => !roles.Contains(t)).ToArray();
                if (rolesToRemove.Any())
                {
                    await _userManager.RemoveFromRolesAsync(identityAppUser, rolesToRemove);
                }
                if (rolesToAdd.Any())
                {
                    await _userManager.AddToRolesAsync(identityAppUser, rolesToAdd);
                }
            }

            await _userManager.UpdateAsync(identityAppUser);
        }

        public async Task Delete(DeleteModel delete)
        {
            var identityAppUser = await _userManager.Users.Include(u => u.UserRoles).ThenInclude(ur => ur.Role).FirstOrDefaultAsync(u => u.Id == delete.Id);

            if (identityAppUser == null) throw new EntryNotFoundException("user", delete.Id);

            await _tokenService.DeleteRefreshTokensForUser(delete.Id);
            await _userManager.DeleteAsync(identityAppUser);
        }
    }
}
