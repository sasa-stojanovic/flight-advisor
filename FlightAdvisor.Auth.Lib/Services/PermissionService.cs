﻿using FlightAdvisor.Common.Exceptions.General;
using System;
using System.Security.Claims;
using FlightAdvisor.Auth.Lib.ServiceInterfaces;
using FlightAdvisor.Common.Constants;
using FlightAdvisor.Auth.Lib.Models;

namespace FlightAdvisor.Auth.Lib.Services
{
    public class PermissionService : IPermissionService
    {
        public void CheckUserGet(ClaimsPrincipal actionUser, int targetUserId)
        {
            CheckAdminOrSelf(actionUser, targetUserId);
        }

        public void CheckUserEdit(ClaimsPrincipal actionUser, UserModel targetUser)
        {
            CheckAdminOrSelf(actionUser, targetUser.Id);
            if (!actionUser.IsInRole(RoleConstants.Admin))
            {
                targetUser.Roles = null;
            }
        }

        public void CheckUserDelete(ClaimsPrincipal actionUser, int targetUserId)
        {
            CheckAdminOrSelf(actionUser, targetUserId);
        }

        private void CheckAdminOrSelf(ClaimsPrincipal actionUser, int targetUserId)
        {
            if (actionUser.IsInRole(RoleConstants.Admin)) return;

            int actionUserId = Convert.ToInt32(actionUser.FindFirstValue(ClaimTypes.NameIdentifier));
            if (actionUserId == targetUserId) return;

            throw new NoPermissionException();
        }
    }
}
