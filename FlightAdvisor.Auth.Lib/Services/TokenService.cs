﻿using FlightAdvisor.Auth.Lib.Configuration;
using FlightAdvisor.Auth.Database.Contexts;
using FlightAdvisor.Auth.Database.Models;
using FlightAdvisor.Auth.Lib.ServiceInterfaces;
using FlightAdvisor.Common.Exceptions.Auth;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;
using FlightAdvisor.Auth.Lib.Models;
using FlightAdvisor.Auth.Lib.Helpers;

namespace FlightAdvisor.Auth.Lib.Services
{
    public class TokenService : ITokenService
    {
        private readonly AuthSettings _authSettings;
        private readonly ApplicationDbContext _dbContext;
        private readonly RoleManager<IdentityAppRole> _roleManager;

        public TokenService(IOptions<AuthSettings> authSettings, ApplicationDbContext dbContext, RoleManager<IdentityAppRole> roleManager)
        {
            _authSettings = authSettings.Value;
            _dbContext = dbContext;
            _roleManager = roleManager;
        }

        public async Task<TokenModel> Refresh(TokenModel token)
        {
            var principal = GetPrincipalFromExpiredToken(token.AccessToken);
            if (!ValidateRefreshToken(token.RefreshToken, principal.Claims))
            {
                throw new AuthInvalidTokenException();
            }
            var tokens = await GenerateTokenForClaims(principal.Claims);
            await DeleteRefreshToken(token.RefreshToken);
            return tokens;
        }

        public async Task Revoke(ClaimsPrincipal actionUser, HttpRequest request)
        {
            if (actionUser.FindFirstValue(ClaimTypes.NameIdentifier) != null)
            {
                int userId = Convert.ToInt32(actionUser.FindFirstValue(ClaimTypes.NameIdentifier));
                await DeleteRefreshTokensForUser(userId);
            }
            else
            {
                GetPrincipalFromExpiredToken(AuthorizationHelper.GetAccessTokenValue(request));
                throw new AuthExpiredTokenException();
            }
        }

        private async Task<TokenModel> GenerateTokenForClaims(IEnumerable<Claim> claims)
        {
            var userId = Convert.ToInt32(claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
            IdentityAppUser identityAppUser = await _dbContext.AppUsers.Include(u => u.UserRoles).ThenInclude(ur => ur.Role).FirstOrDefaultAsync(u => u.Id == userId);
            return await GenerateTokensForUser(identityAppUser);
        }

        public async Task<TokenModel> GenerateTokensForUser(IdentityAppUser identityAppUser)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, identityAppUser.UserName),
                new Claim(JwtRegisteredClaimNames.GivenName, identityAppUser.FirstName),
                new Claim(JwtRegisteredClaimNames.FamilyName, identityAppUser.LastName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.NameIdentifier, identityAppUser.Id.ToString())
            };

            foreach (var userRole in identityAppUser.UserRoles.Select(r => r.Role.Name))
            {
                claims.Add(new Claim(ClaimTypes.Role, userRole));
                var role = await _roleManager.FindByNameAsync(userRole);
                if (role != null)
                {
                    var roleClaims = await _roleManager.GetClaimsAsync(role);
                    foreach (Claim roleClaim in roleClaims)
                    {
                        claims.Add(roleClaim);
                    }
                }
            }

            var accessToken = GenerateAccessToken(claims);
            var refreshToken = await GenerateRefreshToken(claims);
            await DeleteOldRefreshTokens(identityAppUser.Id);
            return new TokenModel() { AccessToken = accessToken, RefreshToken = refreshToken, UserId = identityAppUser.Id };
        }

        private string GenerateAccessToken(IEnumerable<Claim> claims)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_authSettings.Token.Key));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddMinutes(_authSettings.Token.AccessExpireMinutes);

            var token = new JwtSecurityToken(
                _authSettings.Token.Issuer,
                _authSettings.Token.Issuer,
                claims,
                expires: expires,
                signingCredentials: creds
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private async Task<string> GenerateRefreshToken(IEnumerable<Claim> claims)
        {
            string refreshToken = GenerateRandomToken();
            int tokenUserId = Convert.ToInt32(claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);

            IdentityRefreshToken refreshTokenData = new IdentityRefreshToken()
            {
                Token = GetTokenHash(refreshToken),
                User = await _dbContext.AppUsers.FirstOrDefaultAsync(u => u.Id == tokenUserId),
                Issued = DateTime.UtcNow,
                Expires = DateTime.UtcNow.AddMinutes(_authSettings.Token.RefreshExpireMinutes)
            };

            _dbContext.RefreshTokens.Add(refreshTokenData);
            await _dbContext.SaveChangesAsync();

            return refreshToken;
        }

        private bool ValidateRefreshToken(string refreshToken, IEnumerable<Claim> claims)
        {
            byte[] tokenHash = GetTokenHash(refreshToken);
            int tokenUserId = Convert.ToInt32(claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
            return _dbContext.RefreshTokens.Any(rt => rt.Token.Equals(tokenHash) && rt.User.Id == tokenUserId && rt.Expires > DateTime.UtcNow);
        }

        public ClaimsPrincipal GetPrincipalFromExpiredToken(string token)
        {
            try
            {
                var tokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidIssuer = _authSettings.Token.Issuer,
                    ValidateAudience = true,
                    ValidAudience = _authSettings.Token.Issuer,
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_authSettings.Token.Key)),
                    ValidateLifetime = false
                };

                var tokenHandler = new JwtSecurityTokenHandler();
                var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out SecurityToken securityToken);
                var jwtSecurityToken = (JwtSecurityToken)securityToken;
                if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
                    throw new AuthInvalidTokenException();

                return principal;
            }
            catch
            {
                throw new AuthInvalidTokenException();
            }
        }

        public async Task DeleteRefreshTokensForUser(int userId)
        {
            await _dbContext.RefreshTokens.Where(rt => rt.User.Id == userId).DeleteAsync();
        }

        public async Task DeleteRefreshToken(string refreshToken)
        {
            byte[] tokenHash = GetTokenHash(refreshToken);
            await _dbContext.RefreshTokens.Where(rt => rt.Token.Equals(tokenHash)).DeleteAsync();
        }

        private async Task DeleteOldRefreshTokens(int userId)
        {
            await _dbContext.RefreshTokens.Where(rt => rt.User.Id == userId && rt.Expires < DateTime.UtcNow).DeleteAsync();
        }

        private string GenerateRandomToken()
        {
            var randomNumber = new byte[32];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);
                return Convert.ToBase64String(randomNumber);
            }
        }

        private byte[] GetTokenHash(string token)
        {
            using (SHA512 shaM = new SHA512Managed())
            {
                return shaM.ComputeHash(Encoding.UTF8.GetBytes(token));
            }
        }
    }
}
