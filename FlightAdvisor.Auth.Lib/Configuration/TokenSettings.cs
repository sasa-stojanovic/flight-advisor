﻿namespace FlightAdvisor.Auth.Lib.Configuration
{
    public class TokenSettings
    {
        public string Key { get; set; }
        public string Issuer { get; set; }
        public int AccessExpireMinutes { get; set; }
        public int RefreshExpireMinutes { get; set; }
    }
}