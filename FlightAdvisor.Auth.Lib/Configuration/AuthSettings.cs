﻿using System.Collections.Generic;

namespace FlightAdvisor.Auth.Lib.Configuration
{
    public class AuthSettings
    {
        public TokenSettings Token { get; set; }
        public List<InitialUserSettings> InitialUsers { get; set; }
    }
}
