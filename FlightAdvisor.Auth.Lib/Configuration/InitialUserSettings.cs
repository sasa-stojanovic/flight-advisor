﻿namespace FlightAdvisor.Auth.Lib.Configuration
{
    public class InitialUserSettings
    {
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string[] Roles { get; set; }
    }
}