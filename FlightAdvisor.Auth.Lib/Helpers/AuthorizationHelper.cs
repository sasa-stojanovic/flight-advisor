﻿using FlightAdvisor.Auth.Lib.Constants;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using System;
using System.Linq;

namespace FlightAdvisor.Auth.Lib.Helpers
{
    public static class AuthorizationHelper
    {
        public static string GetAccessTokenValue(HttpRequest request)
        {
            if (!request.Headers.TryGetValue(AuthorizationConstants.AuthorizationHeaderName, out StringValues authHeaders)) return null;
            var authHeader = authHeaders.FirstOrDefault(h => h.StartsWith(AuthorizationConstants.AuthorizationHeaderValuePrefix, StringComparison.OrdinalIgnoreCase));
            if (authHeader == null) return null;
            return authHeader.Substring(AuthorizationConstants.AuthorizationHeaderValuePrefix.Length);
        }
    }
}
