﻿using FlightAdvisor.Auth.Lib.Constants;
using FlightAdvisor.Auth.Database.Models;
using FlightAdvisor.Common.Exceptions.Auth;
using Microsoft.AspNetCore.Identity;
using System.Linq;
using System.Threading.Tasks;
using FlightAdvisor.Auth.Lib.Models;

namespace FlightAdvisor.Auth.Lib.Helpers
{
    public static class AccountHelper
    {
        public static IdentityAppUser BusinessToDb(UserModel model)
        {
            return new IdentityAppUser
            {
                Id = model.Id,
                UserName = model.Username,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Active = model.Active
            };
        }

        public static UserModel DbToBusiness(IdentityAppUser identityAppUser)
        {
            return new UserModel
            {
                Id = identityAppUser.Id,
                Username = identityAppUser.UserName,
                FirstName = identityAppUser.FirstName,
                LastName = identityAppUser.LastName,
                Active = identityAppUser.Active,
                Roles = identityAppUser.UserRoles.Select(r => r.Role.Name).ToArray()
            };
        }

        public static async Task ValidateUsername(UserManager<IdentityAppUser> userManager, IdentityAppUser identityAppUser)
        {
            foreach (var userValidator in userManager.UserValidators)
            {
                var usernameValidResult = await userValidator.ValidateAsync(userManager, identityAppUser);
                if (!usernameValidResult.Succeeded)
                {
                    if (usernameValidResult.Errors.Any(e => e.Code == ErrorCodeConstants.DuplicateUserName))
                    {
                        throw new AuthUsernameAlreadyRegisteredException();
                    }
                    else
                    {
                        throw new AuthInvalidUserException(usernameValidResult.Errors);
                    }
                }
            }
        }

        public static async Task ValidatePassword(UserManager<IdentityAppUser> userManager, string password)
        {
            foreach (var passwordValidator in userManager.PasswordValidators)
            {
                var passwordValidResult = await passwordValidator.ValidateAsync(userManager, null, password);
                if (!passwordValidResult.Succeeded)
                {
                    throw new AuthInvalidPasswordException(passwordValidResult.Errors);
                }
            }
        }
    }
}
