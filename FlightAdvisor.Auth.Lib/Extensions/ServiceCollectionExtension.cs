﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using FlightAdvisor.Auth.Lib.Services;
using FlightAdvisor.Auth.Lib.ServiceInterfaces;
using FlightAdvisor.Auth.Lib.Constants;
using FlightAdvisor.Auth.Lib.Configuration;
using FlightAdvisor.Auth.Database.Models;
using Microsoft.AspNetCore.Identity;
using FlightAdvisor.Auth.Database.Contexts;

namespace FlightAdvisor.Auth.Lib.Extensions
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddFlightAdvisorAuthLibServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddFlightAdvisorAuthDatabaseServices();

            var authSettingsSection = configuration.GetSection(ConfigConstants.AuthSettings);
            services.Configure<AuthSettings>(authSettingsSection);

            services.AddIdentity<IdentityAppUser, IdentityAppRole>(options =>
            {
                options.Password.RequiredLength = 8;
                options.Password.RequireLowercase = true;
                options.Password.RequireUppercase = true;
                options.Password.RequireNonAlphanumeric = true;
            }).AddEntityFrameworkStores<ApplicationDbContext>().AddDefaultTokenProviders();

            services.AddScoped<IConfigurationService, ConfigurationService>();
            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<ITokenService, TokenService>();
            services.AddScoped<IPermissionService, PermissionService>();

            return services;
        }
    }
}
