﻿namespace FlightAdvisor.Auth.Lib.Enums
{
    public enum UserSortColumn
    {
        FirstName = 0,
        LastName = 1,
        Username = 2,
        Active = 3
    }
}
