﻿namespace FlightAdvisor.Auth.Lib.Constants
{
    public class AuthorizationConstants
    {
        public const string AuthorizationHeaderName = "Authorization";
        public const string AuthorizationHeaderValuePrefix = "Bearer ";
    }
}
