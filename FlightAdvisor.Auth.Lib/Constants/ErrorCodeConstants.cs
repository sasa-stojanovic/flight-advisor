﻿namespace FlightAdvisor.Auth.Lib.Constants
{
    public static class ErrorCodeConstants
    {
        public const string DuplicateUserName = "DuplicateUserName";
    }
}
