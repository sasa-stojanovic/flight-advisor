﻿using FlightAdvisor.Auth.Database.Models;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using FlightAdvisor.Auth.Lib.Models;

namespace FlightAdvisor.Auth.Lib.ServiceInterfaces
{
    public interface ITokenService
    {
        Task<TokenModel> Refresh(TokenModel tokenData);
        Task Revoke(ClaimsPrincipal actionUser, HttpRequest request);
        Task<TokenModel> GenerateTokensForUser(IdentityAppUser identityAppUser);
        ClaimsPrincipal GetPrincipalFromExpiredToken(string token);
        Task DeleteRefreshTokensForUser(int userId);
        Task DeleteRefreshToken(string refreshToken);
    }
}
