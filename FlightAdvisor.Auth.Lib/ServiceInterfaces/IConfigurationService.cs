﻿namespace FlightAdvisor.Auth.Lib.ServiceInterfaces
{
    public interface IConfigurationService
    {
        void EnsureDatabaseCreated();
        void EnsureRolesCreated();
        void EnsureInitialUsersCreated();
    }
}
