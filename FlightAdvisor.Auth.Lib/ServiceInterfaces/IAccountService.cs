﻿using System.Threading.Tasks;
using FlightAdvisor.Auth.Lib.Models;
using FlightAdvisor.Common.Models;
using FlightAdvisor.Auth.Lib.Enums;

namespace FlightAdvisor.Auth.Lib.ServiceInterfaces
{
    public interface IAccountService
    {
        Task<TokenModel> Login(LoginModel loginData);
        Task Logout(LogoutModel logoutData);
        Task<TokenModel> Register(UserModel userData);
        Task<UserModel> Get(GetModel getData);
        Task<ListResultModel<UserModel>> List(ListModel<UserSortColumn> listData);
        Task SetStatus(UserStatusModel userStatusData);
        Task Edit(UserModel userData);
        Task Delete(DeleteModel deleteData);
    }
}
