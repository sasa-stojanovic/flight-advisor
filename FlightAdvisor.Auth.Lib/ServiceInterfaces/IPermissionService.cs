﻿using FlightAdvisor.Auth.Lib.Models;
using System.Security.Claims;

namespace FlightAdvisor.Auth.Lib.ServiceInterfaces
{
    public interface IPermissionService
    {
        void CheckUserGet(ClaimsPrincipal actionUser, int targetUserId);
        void CheckUserEdit(ClaimsPrincipal actionUser, UserModel targetUser);
        void CheckUserDelete(ClaimsPrincipal actionUser, int targetUserId);
    }
}
