﻿using System.ComponentModel.DataAnnotations;

namespace FlightAdvisor.Auth.Lib.Models
{
    public class UserStatusModel
    {
        [Required]
        public int UserId { get; set; }

        [Required]
        public bool? Active { get; set; }
    }
}
