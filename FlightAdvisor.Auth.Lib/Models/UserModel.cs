﻿using System.ComponentModel.DataAnnotations;

namespace FlightAdvisor.Auth.Lib.Models
{
    public class UserModel
    {
        public int Id { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string Username { get; set; }

        public string Password { get; set; }

        public string[] Roles { get; set; }

        public bool Active { get; set; } = true;
    }
}
