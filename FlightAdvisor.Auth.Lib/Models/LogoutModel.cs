﻿using System.ComponentModel.DataAnnotations;

namespace FlightAdvisor.Auth.Lib.Models
{
    public class LogoutModel
    {
        [Required]
        public string RefreshToken { get; set; }
    }
}
