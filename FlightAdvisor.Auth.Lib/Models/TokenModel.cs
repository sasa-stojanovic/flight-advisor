﻿using System.ComponentModel.DataAnnotations;

namespace FlightAdvisor.Auth.Lib.Models
{
    public class TokenModel
    {
        [Required]
        public string AccessToken { get; set; }

        [Required]
        public string RefreshToken { get; set; }

        public int UserId { get; set; }
    }
}
