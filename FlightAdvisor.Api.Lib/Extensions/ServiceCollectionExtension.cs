﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using FlightAdvisor.Api.Lib.Services;
using FlightAdvisor.Api.Lib.ServiceInterfaces;
using FlightAdvisor.Api.Lib.Constants;
using FlightAdvisor.Api.Lib.Configuration;
using FlightAdvisor.Api.Database.Extensions;

namespace FlightAdvisor.Api.Lib.Extensions
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddFlightAdvisorApiLibServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddFlightAdvisorApiDatabaseServices();

            var libSettingsSection = configuration.GetSection(ConfigConstants.LibSettings);
            services.Configure<LibSettings>(libSettingsSection);
            var libSettings = libSettingsSection.Get<LibSettings>();

            if (libSettings.Redis.Enabled)
            {
                services.AddDistributedRedisCache(option =>
                {
                    option.Configuration = libSettings.Redis.Configuration;
                    option.InstanceName = libSettings.Redis.InstanceName;
                });
            }
            else
            {
                services.AddDistributedMemoryCache();
            }

            services.AddScoped<IConfigurationService, ConfigurationService>();
            services.AddScoped<IAirlineService, AirlineService>();
            services.AddScoped<IAirportService, AirportService>();
            services.AddScoped<ICityService, CityService>();
            services.AddScoped<ICommentService, CommentService>();
            services.AddScoped<IRouteService, RouteService>();
            services.AddScoped<ITravelService, TravelService>();
            services.AddScoped<ICacheService, CacheService>();
            services.AddScoped<ITicketService, TicketService>();

            return services;
        }
    }
}
