﻿namespace FlightAdvisor.Api.Lib.Constants
{
    public static class CacheConstants
    {
        public const string Airports = "Airports";
        public const string Routes = "Routes";
        public const string EstimatedPricePerKilometer = "EstimatedPricePerKilometer";
    }
}
