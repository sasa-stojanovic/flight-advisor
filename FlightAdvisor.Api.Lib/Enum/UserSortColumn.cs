﻿namespace FlightAdvisor.Api.Lib.Enums
{
    public enum CitySortColumn
    {
        Name = 0,
        Country = 1
    }
}
