﻿using FlightAdvisor.Api.Database.Models;
using FlightAdvisor.Api.Lib.Models;

namespace FlightAdvisor.Api.Lib.Helpers
{
    public static class CommentHelper
    {
        public static CommentModel DbToBusiness(Comment commentDb)
        {
            return new CommentModel()
            {
                Id = commentDb.Id,
                CityId = commentDb.CityId,
                Text = commentDb.Text,
                UserId = commentDb.UserId,
                User = commentDb.User,
                CreatedDate = commentDb.CreatedDate,
                ModifyDate = commentDb.ModifyDate
            };
        }

        public static Comment BusinessToDb(CommentModel comment)
        {
            return new Comment()
            {
                Id = comment.Id,
                CityId = comment.CityId,
                Text = comment.Text,
                UserId = comment.UserId,
                User = comment.User,
                CreatedDate = comment.CreatedDate,
                ModifyDate = comment.ModifyDate
            };
        }
    }
}
