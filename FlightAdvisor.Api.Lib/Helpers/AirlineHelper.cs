﻿using FlightAdvisor.Api.Database.Models;
using FlightAdvisor.Api.Lib.Constants;
using FlightAdvisor.Api.Lib.Models;
using System;
using System.Collections.Generic;

namespace FlightAdvisor.Api.Lib.Helpers
{
    public static class AirlineHelper
    {
        public static Airline FromString(string routeString, List<int> airportIds)
        {
            if (string.IsNullOrEmpty(routeString)) return null;

            string[] routeData = routeString.Split(',');
            int souceAirportId = Convert.ToInt32(routeData[3]);
            int destinationAirportId = Convert.ToInt32(routeData[5]);

            if (!airportIds.Contains(souceAirportId) || !airportIds.Contains(destinationAirportId)) return null;

            return new Airline()
            {
                Id = routeData[1] != ParsingConstants.MissingDataString ? Convert.ToInt32(routeData[1]) : 0,
                Code = routeData[0] != ParsingConstants.MissingDataString ? routeData[0] : null
            };
        }

        public static AirlineModel DbToBusiness(Airline airlineDb)
        {
            return new AirlineModel()
            {
                Id = airlineDb.Id,
                Code = airlineDb.Code
            };
        }
    }
}
