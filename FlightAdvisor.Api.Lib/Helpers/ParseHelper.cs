﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace FlightAdvisor.Api.Lib.Helpers
{
    public static class ParseHelper
    {
        public static string[] SplitFileInputLine(string line)
        {
            return Regex.Matches(line, "(\"[^\"\\\\]*(?:\\\\.[^\"\\\\]*)*\")|[^,]+").Cast<Match>().Select(m => m.Value.Trim('"')).ToArray();
        }
    }
}
