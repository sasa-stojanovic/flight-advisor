﻿using FlightAdvisor.Api.Database.Models;
using FlightAdvisor.Api.Lib.Constants;
using FlightAdvisor.Api.Lib.Models;
using Geolocation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FlightAdvisor.Api.Lib.Helpers
{
    public static class AirportHelper
    {
        public static AirportModel DbToBusiness(Airport airportDb)
        {
            return new AirportModel()
            {
                Id = airportDb.Id,
                Name = airportDb.Name,
                IATA = airportDb.IATA,
                ICAO = airportDb.ICAO,
                Location = new Coordinate() { Latitude = airportDb.Latitude, Longitude = airportDb.Longitude },
                Altitude = airportDb.Altitude,
                Timezone = airportDb.Timezone,
                DST = airportDb.DST,
                Tz = airportDb.Tz,
                Type = airportDb.Type,
                Source = airportDb.Source
            };
        }

        public static Airport FromString(string airportString, List<CityModel> cities)
        {
            if (string.IsNullOrEmpty(airportString)) return null;

            string[] airportData = ParseHelper.SplitFileInputLine(airportString);
            var city = cities.FirstOrDefault(c => c.Name == airportData[2] && c.Country == airportData[3]);

            if (city == null) return null;

            return new Airport()
            {
                Id = Convert.ToInt32(airportData[0]),
                Name = airportData[1] != ParsingConstants.MissingDataString ? airportData[1] : null,
                CityId = city.Id,
                IATA = airportData[4] != ParsingConstants.MissingDataString ? airportData[4] : null,
                ICAO = airportData[5] != ParsingConstants.MissingDataString ? airportData[5] : null,
                Latitude = Convert.ToDouble(airportData[6]),
                Longitude = Convert.ToDouble(airportData[7]),
                Altitude = Convert.ToDecimal(airportData[8]),
                Timezone = airportData[9] != ParsingConstants.MissingDataString ? (decimal?)Convert.ToDecimal(airportData[9]) : null,
                DST = airportData[10] != ParsingConstants.MissingDataString ? (char?)char.Parse(airportData[10]) : null,
                Tz = airportData[11] != ParsingConstants.MissingDataString ? airportData[11] : null,
                Type = airportData[12] != ParsingConstants.MissingDataString ? airportData[12] : null,
                Source = airportData[13] != ParsingConstants.MissingDataString ? airportData[13] : null
            };
        }
    }
}
