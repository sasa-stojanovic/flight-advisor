﻿using FlightAdvisor.Api.Database.Models;
using FlightAdvisor.Api.Lib.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FlightAdvisor.Api.Lib.Helpers
{
    public static class RouteHelper
    {
        public static Route FromString(string routeString, List<AirlineModel> airlines, List<int> airportIds)
        {
            if (string.IsNullOrEmpty(routeString)) return null;

            string[] routeData = routeString.Split(',');
            int souceAirportId = Convert.ToInt32(routeData[3]);
            int destinationAirportId = Convert.ToInt32(routeData[5]);

            if (!airportIds.Contains(souceAirportId) || !airportIds.Contains(destinationAirportId)) return null;

            return new Route()
            {
                AirlineId = airlines.First(a => a.Code == routeData[0]).Id,
                SourceAirportId = souceAirportId,
                DestinationAirportId = destinationAirportId,
                Codeshare = routeData[6] == "Y",
                Stops = Convert.ToInt32(routeData[7]),
                Equipment = routeData[8],
                Price = Convert.ToDecimal(routeData[9])
            };
        }
    }
}
