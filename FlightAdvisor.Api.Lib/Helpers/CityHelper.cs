﻿using FlightAdvisor.Api.Database.Models;
using FlightAdvisor.Api.Lib.Constants;
using FlightAdvisor.Api.Lib.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FlightAdvisor.Api.Lib.Helpers
{
    public static class CityHelper
    {
        public static CityModel DbToBusiness(City cityDb)
        {
            return new CityModel()
            {
                Id = cityDb.Id,
                Name = cityDb.Name,
                Country = cityDb.Country,
                Description = cityDb.Description,
                Airports = cityDb.Airports != null ? cityDb.Airports.Select(a => AirportHelper.DbToBusiness(a)).ToList() : null,
                Comments = cityDb.Comments != null ? cityDb.Comments.Select(c => CommentHelper.DbToBusiness(c)).ToList() : null
            };
        }

        public static City BusinessToDb(CityModel city)
        {
            return new City()
            {
                Id = city.Id,
                Name = city.Name,
                Country = city.Country,
                Description = city.Description
            };
        }

        public static City FromString(string cityString)
        {
            if (string.IsNullOrEmpty(cityString)) return null;

            string[] cityData = ParseHelper.SplitFileInputLine(cityString);

            return new City()
            {

                Name = cityData[2] != ParsingConstants.MissingDataString ? cityData[2] : null,
                Country = cityData[3] != ParsingConstants.MissingDataString ? cityData[3] : null,
                Description = "no description"
            };
        }
    }
}
