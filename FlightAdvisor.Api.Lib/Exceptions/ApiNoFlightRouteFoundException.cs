﻿using System;

namespace FlightAdvisor.Api.Lib.Exceptions
{
    public class ApiNoFlightRouteFoundException : Exception
    {
        public ApiNoFlightRouteFoundException()
        {
        }

        public ApiNoFlightRouteFoundException(string message) : base(message)
        {
        }

        public ApiNoFlightRouteFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
