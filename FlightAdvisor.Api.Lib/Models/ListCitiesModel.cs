﻿using FlightAdvisor.Common.Models;

namespace FlightAdvisor.Api.Lib.Models
{
    public class ListCitiesModel<T> : ListModel<T>
    {
        public int? Comments { get; set; }
    }
}
