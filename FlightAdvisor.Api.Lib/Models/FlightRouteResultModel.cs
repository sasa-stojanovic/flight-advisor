﻿using System.Collections.Generic;

namespace FlightAdvisor.Api.Lib.Models
{
    public class FlightRouteResultModel
    {
        public decimal Price { get; set; }
        public double Length { get; set; }
        public string SourceCity { get; set; }
        public string DestinationCity { get; set; }
        public List<RouteSimpleResultModel> Route { get; set; }
    }
}
