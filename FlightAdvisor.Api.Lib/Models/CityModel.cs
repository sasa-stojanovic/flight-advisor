﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FlightAdvisor.Api.Lib.Models
{
    public class CityModel
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Country { get; set; }

        [Required]
        public string Description { get; set; }

        public List<AirportModel> Airports { get; set; }

        public List<CommentModel> Comments { get; set; }
    }
}
