﻿using Geolocation;
using System.ComponentModel.DataAnnotations;

namespace FlightAdvisor.Api.Lib.Models
{
    public class AirlineModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
    }
}
