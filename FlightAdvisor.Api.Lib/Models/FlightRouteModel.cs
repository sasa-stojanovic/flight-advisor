﻿using System.ComponentModel.DataAnnotations;

namespace FlightAdvisor.Api.Lib.Models
{
    public class FlightRouteModel
    {
        [Required]
        public int SourceCityId { get; set; }

        [Required]
        public int DestinationCityId { get; set; }
    }
}
