﻿namespace FlightAdvisor.Api.Lib.Models
{
    public class RouteCacheModel
    {
        public int SourceAirportId { get; set; }
        public int DestinationAirportId { get; set; }
        public decimal Price { get; set; }
    }
}
