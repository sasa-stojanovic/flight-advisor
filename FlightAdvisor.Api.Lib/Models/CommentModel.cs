﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FlightAdvisor.Api.Lib.Models
{
    public class CommentModel
    {
        public int Id { get; set; }

        [Required]
        public string Text { get; set; }

        [Required]
        public int CityId { get; set; }

        public int UserId { get; set; }

        public string User { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime ModifyDate { get; set; }

        public CityModel City { get; set; }
    }
}
