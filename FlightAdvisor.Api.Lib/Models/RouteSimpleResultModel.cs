﻿using System.Text.Json.Serialization;

namespace FlightAdvisor.Api.Lib.Models
{
    public class RouteSimpleResultModel
    {
        public string Source { get; set; }
        public string Destination { get; set; }
        public decimal Price { get; set; }
        public double Length { get; set; }
        [JsonIgnore]
        public int SourceCityId { get; set; }
        [JsonIgnore]
        public int DestinationCityId { get; set; }
    }
}
