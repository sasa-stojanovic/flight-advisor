﻿using Geolocation;

namespace FlightAdvisor.Api.Lib.Models
{
    public class AirportRouteModel : AirportCacheModel
    {
        public int RouteId { get; set; }
        public decimal RoutePrice { get; set; }
        public decimal EstimatedRemainingRoutesPrice { get; set; }
        public AirportRouteModel PreviousAirport { get; set; }
        public bool Processed { get; set; } = false;
        public bool SourceAirport { get; set; } = false;

        public decimal TotalEstimatedRoutesPrice => AllRoutesPrice + EstimatedRemainingRoutesPrice;
        public decimal AllRoutesPrice => SourceAirport ? 0 : PreviousAirport != null ? PreviousAirport.AllRoutesPrice + RoutePrice : decimal.MaxValue;

        public AirportRouteModel(AirportCacheModel airportCache)
        {
            Id = airportCache.Id;
            Location = new Coordinate() { Latitude = airportCache.Location.Latitude, Longitude = airportCache.Location.Longitude };
        }
    }
}
