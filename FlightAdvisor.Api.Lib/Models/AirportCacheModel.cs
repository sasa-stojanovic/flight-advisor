﻿using Geolocation;

namespace FlightAdvisor.Api.Lib.Models
{
    public class AirportCacheModel
    {
        public int Id { get; set; }
        public Coordinate Location { get; set; }
    }
}
