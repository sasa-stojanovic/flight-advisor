﻿using Geolocation;
using System.ComponentModel.DataAnnotations;

namespace FlightAdvisor.Api.Lib.Models
{
    public class AirportModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public CityModel City { get; set; }
        public string IATA { get; set; }
        public string ICAO { get; set; }
        public Coordinate Location { get; set; }
        public decimal Altitude { get; set; }
        public decimal? Timezone { get; set; }
        public char? DST { get; set; }
        public string Tz { get; set; }
        public string Type { get; set; }
        public string Source { get; set; }
    }
}
