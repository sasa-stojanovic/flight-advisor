﻿namespace FlightAdvisor.Api.Lib.Configuration
{
    public class LibSettings
    {
        public ImportSettings Import { get; set; }
        public RedisSettings Redis { get; set; }
    }
}
