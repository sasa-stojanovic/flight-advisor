﻿namespace FlightAdvisor.Api.Lib.Configuration
{
    public class RedisSettings
    {
        public bool Enabled { get; set; } = false;
        public string Configuration { get; set; }
        public string InstanceName { get; set; }
    }
}
