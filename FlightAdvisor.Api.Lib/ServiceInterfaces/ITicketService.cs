﻿using Geolocation;
using System.Threading.Tasks;

namespace FlightAdvisor.Api.Lib.ServiceInterfaces
{
    public interface ITicketService
    {
        Task RecalculateEstimatedTicketsPricePerKilometer();
        Task<decimal> EstimatedTicketsPriceBetweenTwoPoints(Coordinate source, Coordinate destination);
    }
}
