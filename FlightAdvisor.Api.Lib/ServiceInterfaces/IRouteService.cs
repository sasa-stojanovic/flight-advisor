﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace FlightAdvisor.Api.Lib.ServiceInterfaces
{
    public interface IRouteService
    {
        Task ImportRoutes(IFormFile file);
        Task RefreshCache();
    }
}
