﻿using FlightAdvisor.Api.Lib.Models;
using FlightAdvisor.Common.Models;
using System.Security.Claims;
using System.Threading.Tasks;

namespace FlightAdvisor.Api.Lib.ServiceInterfaces
{
    public interface ICommentService
    {
        Task<int> Create(CommentModel comment, ClaimsPrincipal actionUser);
        Task Edit(CommentModel comment, ClaimsPrincipal actionUser);
        Task Delete(DeleteModel delete, ClaimsPrincipal actionUser);
    }
}
