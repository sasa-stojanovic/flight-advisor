﻿using System.Threading.Tasks;

namespace FlightAdvisor.Api.Lib.ServiceInterfaces
{
    public interface IConfigurationService
    {
        void EnsureDatabaseCreated();
        Task RefreshCache();
    }
}
