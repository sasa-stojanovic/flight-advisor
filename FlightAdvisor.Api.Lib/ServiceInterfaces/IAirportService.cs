﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace FlightAdvisor.Api.Lib.ServiceInterfaces
{
    public interface IAirportService
    {
        Task ImportAirports(IFormFile file);
        Task RefreshCache();
    }
}
