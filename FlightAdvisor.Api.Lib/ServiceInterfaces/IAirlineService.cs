﻿using FlightAdvisor.Api.Lib.Models;
using FlightAdvisor.Common.Models;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace FlightAdvisor.Api.Lib.ServiceInterfaces
{
    public interface IAirlineService
    {
        Task ImportAirlines(IFormFile file);
        Task<ListResultModel<AirlineModel>> List(ListModel list);
    }
}
