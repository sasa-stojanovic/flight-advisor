﻿using FlightAdvisor.Api.Lib.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FlightAdvisor.Api.Lib.ServiceInterfaces
{
    public interface ICacheService
    {
        Task SaveAirports(List<AirportCacheModel> airports);
        Task<List<AirportCacheModel>> GetAirports();
        Task SaveRoutes(List<RouteCacheModel> routes);
        Task<List<RouteCacheModel>> GetRoutes();
        Task SaveEstimatedPricePerKilometer(decimal routes);
        Task<decimal> GetEstimatedPricePerKilometer();
    }
}
