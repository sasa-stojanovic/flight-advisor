﻿using FlightAdvisor.Api.Lib.Enums;
using FlightAdvisor.Api.Lib.Models;
using FlightAdvisor.Common.Models;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace FlightAdvisor.Api.Lib.ServiceInterfaces
{
    public interface ICityService
    {
        Task<CityModel> Get(int id);
        Task<CityModel> GetForAirport(int airportId);
        Task<int> Create(CityModel city);
        Task<ListResultModel<CityModel>> List(ListCitiesModel<CitySortColumn> list);
        Task ImportCities(IFormFile file);
    }
}
