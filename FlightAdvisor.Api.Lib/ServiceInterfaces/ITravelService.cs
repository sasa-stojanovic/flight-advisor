﻿using FlightAdvisor.Api.Lib.Models;
using System.Threading.Tasks;

namespace FlightAdvisor.Api.Lib.ServiceInterfaces
{
    public interface ITravelService
    {
        Task<FlightRouteResultModel> GetCheapestFlightRoute(FlightRouteModel flightRoute);
        Task<FlightRouteResultModel> GetOptimalFlightRoute(FlightRouteModel flightRoute);
    }
}
