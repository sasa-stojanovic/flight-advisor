﻿using FlightAdvisor.Api.Lib.ServiceInterfaces;
using FlightAdvisor.Api.Database.Contexts;
using System.Threading.Tasks;
using System;
using FlightAdvisor.Api.Lib.Models;
using System.Collections.Generic;
using Geolocation;
using System.Linq;

namespace FlightAdvisor.Api.Lib.Services
{
    public class TicketService : ITicketService
    {
        private readonly ICacheService _cacheService;

        public TicketService(ICacheService cacheService)
        {
            _cacheService = cacheService;
        }

        public async Task RecalculateEstimatedTicketsPricePerKilometer()
        {
            List<AirportCacheModel> airports = await _cacheService.GetAirports();
            List<RouteCacheModel> routes = await _cacheService.GetRoutes();

            decimal totalPrice = 0;
            double totalDistance = 0;

            foreach (var route in routes)
            {
                totalPrice += route.Price;

                Coordinate source = airports.First(a => a.Id == route.SourceAirportId).Location;
                Coordinate destination = airports.First(a => a.Id == route.DestinationAirportId).Location;
                totalDistance += GeoCalculator.GetDistance(source, destination, 2, DistanceUnit.Kilometers);
            }

            decimal pricePerKilometer = totalDistance > 0 ? totalPrice / (decimal)totalDistance : 0;

            await _cacheService.SaveEstimatedPricePerKilometer(pricePerKilometer);
        }

        public async Task<decimal> EstimatedTicketsPriceBetweenTwoPoints(Coordinate source, Coordinate destination)
        {
            decimal estimatedPricePerKilometer = await _cacheService.GetEstimatedPricePerKilometer();
            double distance = GeoCalculator.GetDistance(source, destination, 2, DistanceUnit.Kilometers);
            return estimatedPricePerKilometer * (decimal)distance;
        }
    }
}
