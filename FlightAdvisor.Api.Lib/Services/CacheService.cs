﻿using FlightAdvisor.Api.Lib.Constants;
using FlightAdvisor.Api.Lib.Models;
using FlightAdvisor.Api.Lib.ServiceInterfaces;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FlightAdvisor.Api.Lib.Services
{
    public class CacheService : ICacheService
    {
        private readonly IDistributedCache _distributedCache;

        public CacheService(IDistributedCache distributedCache)
        {
            _distributedCache = distributedCache;
        }

        public async Task SaveAirports(List<AirportCacheModel> airports)
        {
            await _distributedCache.SetStringAsync(CacheConstants.Airports, JsonConvert.SerializeObject(airports));
        }

        public async Task<List<AirportCacheModel>> GetAirports()
        {
            string airportsString = await _distributedCache.GetStringAsync(CacheConstants.Airports);

            if (string.IsNullOrEmpty(airportsString)) return null;

            return JsonConvert.DeserializeObject<List<AirportCacheModel>>(airportsString);
        }

        public async Task SaveRoutes(List<RouteCacheModel> routes)
        {
            await _distributedCache.SetStringAsync(CacheConstants.Routes, JsonConvert.SerializeObject(routes));
        }

        public async Task<List<RouteCacheModel>> GetRoutes()
        {
            string routesString = await _distributedCache.GetStringAsync(CacheConstants.Routes);

            if (string.IsNullOrEmpty(routesString)) return null;

            return JsonConvert.DeserializeObject<List<RouteCacheModel>>(routesString);
        }

        public async Task SaveEstimatedPricePerKilometer(decimal estimatedPricePerKilometer)
        {
            await _distributedCache.SetStringAsync(CacheConstants.EstimatedPricePerKilometer, estimatedPricePerKilometer.ToString());
        }

        public async Task<decimal> GetEstimatedPricePerKilometer()
        {
            string estimatedPricePerKilometerString = await _distributedCache.GetStringAsync(CacheConstants.EstimatedPricePerKilometer);

            if (string.IsNullOrEmpty(estimatedPricePerKilometerString)) return 0;

            return Convert.ToDecimal(estimatedPricePerKilometerString);
        }
    }
}
