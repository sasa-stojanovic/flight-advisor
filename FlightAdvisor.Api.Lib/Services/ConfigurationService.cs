﻿using Microsoft.EntityFrameworkCore;
using FlightAdvisor.Api.Lib.ServiceInterfaces;
using FlightAdvisor.Api.Database.Contexts;
using System.Threading.Tasks;

namespace FlightAdvisor.Api.Lib.Services
{
    public class ConfigurationService : IConfigurationService
    {
        private readonly FlightAdvisorApiDbContext _flightAdvisorApiDbContext;
        private readonly IAirportService _airportService;
        private readonly IRouteService _routeService;

        public ConfigurationService(FlightAdvisorApiDbContext flightAdvisorApiDbContext, IAirportService airportService, IRouteService routeService)
        {
            _flightAdvisorApiDbContext = flightAdvisorApiDbContext;
            _airportService = airportService;
            _routeService = routeService;
        }

        public void EnsureDatabaseCreated()
        {
            _flightAdvisorApiDbContext.Database.Migrate();
        }

        public async Task RefreshCache()
        {
            await _airportService.RefreshCache();
            await _routeService.RefreshCache();
        }
    }
}
