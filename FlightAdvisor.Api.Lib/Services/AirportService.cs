﻿using FlightAdvisor.Api.Lib.ServiceInterfaces;
using FlightAdvisor.Api.Database.Contexts;
using System.Threading.Tasks;
using FlightAdvisor.Api.Lib.Models;
using FlightAdvisor.Api.Lib.Helpers;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Collections.Generic;
using FlightAdvisor.Api.Database.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using Geolocation;
using Microsoft.EntityFrameworkCore;
using FlightAdvisor.Api.Lib.Enums;
using FlightAdvisor.Api.Lib.Configuration;
using Microsoft.Extensions.Options;

namespace FlightAdvisor.Api.Lib.Services
{
    public class AirportService : IAirportService
    {
        private readonly FlightAdvisorApiDbContext _flightAdvisorApiDbContext;
        private readonly ICityService _cityService;
        private readonly ICacheService _cacheService;
        private readonly LibSettings _libSettings;
        private readonly ILogger<AirportService> _logger;

        public AirportService(FlightAdvisorApiDbContext flightAdvisorApiDbContext, ICityService cityService, ICacheService cacheService, IOptions<LibSettings> libSettings, ILogger<AirportService> logger)
        {
            _flightAdvisorApiDbContext = flightAdvisorApiDbContext;
            _cityService = cityService;
            _cacheService = cacheService;
            _libSettings = libSettings.Value;
            _logger = logger;
        }

        public async Task ImportAirports(IFormFile file)
        {
            if (_libSettings.Import.ImportCities)
            {
                await _cityService.ImportCities(file);
            }

            await _flightAdvisorApiDbContext.Database.ExecuteSqlRawAsync("DELETE FROM Airlines");
            await _flightAdvisorApiDbContext.Database.ExecuteSqlRawAsync("DELETE FROM Routes");
            await _flightAdvisorApiDbContext.Database.ExecuteSqlRawAsync("DELETE FROM Airports");

            List<CityModel> cities = (await _cityService.List(new ListCitiesModel<CitySortColumn>() { Comments = 0 })).Data;

            using (var reader = new StreamReader(file.OpenReadStream()))
            {
                while (reader.Peek() >= 0)
                {
                    try
                    {
                        Airport airport = AirportHelper.FromString(await reader.ReadLineAsync(), cities);
                        if (airport != null)
                        {
                            _flightAdvisorApiDbContext.Airports.Add(airport);
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex, ex.Message);
                    }
                }
            }
            await _flightAdvisorApiDbContext.SaveChangesAsync();

            await RefreshCache();
        }

        public async Task RefreshCache()
        {
            List<AirportCacheModel> airports = _flightAdvisorApiDbContext.Airports.Select(a => new AirportCacheModel() { Id = a.Id, Location = new Coordinate() { Latitude = a.Latitude, Longitude = a.Longitude } }).ToList();
            await _cacheService.SaveAirports(airports);
        }
    }
}
