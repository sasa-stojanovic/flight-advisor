﻿using FlightAdvisor.Api.Lib.ServiceInterfaces;
using System.Threading.Tasks;
using FlightAdvisor.Api.Lib.Models;
using System.Collections.Generic;
using System.Linq;
using Geolocation;
using System;
using FlightAdvisor.Api.Lib.Exceptions;
using FlightAdvisor.Common.Exceptions.General;

namespace FlightAdvisor.Api.Lib.Services
{
    public class TravelService : ITravelService
    {
        private readonly ICityService _cityService;
        private readonly ITicketService _ticketService;
        private readonly ICacheService _cacheService;

        public TravelService(ICityService cityService, ITicketService ticketService, ICacheService cacheService)
        {
            _cityService = cityService;
            _ticketService = ticketService;
            _cacheService = cacheService;
        }

        /// <summary>
        /// Cheapest flight route calculation based on Dijkstra's algorithm
        /// </summary>
        /// <param name="flightRoute">Desired flight route</param>
        /// <returns>Cheapest flight route plan</returns>
        public async Task<FlightRouteResultModel> GetCheapestFlightRoute(FlightRouteModel flightRoute)
        {
            // get cities
            CityModel sourceCity = await _cityService.Get(flightRoute.SourceCityId);
            CityModel destinationCity = await _cityService.Get(flightRoute.DestinationCityId);

            if (!sourceCity.Airports.Any() || !destinationCity.Airports.Any()) throw new ApiNoFlightRouteFoundException();

            // get airports and routes from cache
            List<AirportCacheModel> airports = await _cacheService.GetAirports();
            List<RouteCacheModel> routes = await _cacheService.GetRoutes();

            // get source and destination airports
            AirportRouteModel destinationAirport = new AirportRouteModel(airports.First(a => a.Id == destinationCity.Airports.First().Id));
            AirportRouteModel sourceAirport = new AirportRouteModel(airports.First(a => a.Id == sourceCity.Airports.First().Id));
            sourceAirport.SourceAirport = true;

            // add source and destination airports to relevant airports
            List<AirportRouteModel> relevantAirports = new List<AirportRouteModel>();
            relevantAirports.Add(sourceAirport);
            relevantAirports.Add(destinationAirport);

            // start from source airport
            var currentAirport = sourceAirport;

            // while destination is not reached and airports for scaning are available
            while (currentAirport != null)
            {
                // process all routes for current airport
                foreach (var route in routes.Where(r => r.SourceAirportId == currentAirport.Id))
                {
                    // get route destination airport from relevant airport list
                    AirportRouteModel nextAirport = relevantAirports.FirstOrDefault(a => a.Id == route.DestinationAirportId);

                    // if route destination airport is not in relevant list
                    if (nextAirport == null)
                    {
                        // get route destination airport
                        nextAirport = new AirportRouteModel(airports.First(a => a.Id == route.DestinationAirportId));
                        // add route destination to relevant airports
                        relevantAirports.Add(nextAirport);
                    }

                    // calculate new price to get from start airport to route destination airport
                    var newNextAirportPrice = currentAirport.AllRoutesPrice + route.Price;
                    // if old price is larger than new price 
                    if (nextAirport.AllRoutesPrice > newNextAirportPrice)
                    {
                        // update route destination airport previous airport
                        nextAirport.PreviousAirport = currentAirport;
                        // ticket price from previous airport
                        nextAirport.RoutePrice = route.Price;
                    }
                }

                // mark airport as processed in order not to process it again
                currentAirport.Processed = true;

                // filter relevant airports by removing final destination airport, processed ones and ones which price to get from start airport to it is larger than current lowest price to final destination airport
                // next airport for processing should be the one that has the lowest current route price
                currentAirport = relevantAirports.Where(a => a.Id != destinationAirport.Id && !a.Processed && a.AllRoutesPrice < destinationAirport.AllRoutesPrice).OrderBy(a => a.AllRoutesPrice).FirstOrDefault();
            }

            if (destinationAirport.AllRoutesPrice == decimal.MaxValue) throw new ApiNoFlightRouteFoundException();

            return await PrepareFlightRouteResult(sourceCity, destinationCity, sourceAirport, destinationAirport);
        }

        /// <summary>
        /// Cheapest flight route calculation based on A* algorithm
        /// </summary>
        /// <param name="flightRoute">Desired flight route</param>
        /// <returns>Optimal flight route plan</returns>
        public async Task<FlightRouteResultModel> GetOptimalFlightRoute(FlightRouteModel flightRoute)
        {
            // get cities
            CityModel sourceCity = await _cityService.Get(flightRoute.SourceCityId);
            CityModel destinationCity = await _cityService.Get(flightRoute.DestinationCityId);

            if (!sourceCity.Airports.Any() || !destinationCity.Airports.Any()) throw new ApiNoFlightRouteFoundException();

            // get airports and routes from cache
            List<AirportCacheModel> airports = await _cacheService.GetAirports();
            List<RouteCacheModel> routes = await _cacheService.GetRoutes();

            // get source and destination airports
            AirportRouteModel destinationAirport = new AirportRouteModel(airports.First(a => a.Id == destinationCity.Airports.First().Id));
            AirportRouteModel sourceAirport = new AirportRouteModel(airports.First(a => a.Id == sourceCity.Airports.First().Id));
            sourceAirport.SourceAirport = true;
            sourceAirport.EstimatedRemainingRoutesPrice = await _ticketService.EstimatedTicketsPriceBetweenTwoPoints(sourceAirport.Location, destinationAirport.Location);
            
            // add source and destination airports to relevant airports
            List<AirportRouteModel> relevantAirports = new List<AirportRouteModel>();
            relevantAirports.Add(sourceAirport);
            relevantAirports.Add(destinationAirport);

            // start from source airport
            var currentAirport = sourceAirport;

            // set destination reached flag
            bool destinationReached = false;

            // while destination is not reached and airports for scaning are available
            while (!destinationReached && currentAirport != null)
            {
                // process all routes for current airport
                foreach (var route in routes.Where(r => r.SourceAirportId == currentAirport.Id).OrderBy(r => r.Price))
                {
                    // get route destination airport from relevant airport list
                    AirportRouteModel nextAirport = relevantAirports.FirstOrDefault(a => a.Id == route.DestinationAirportId);

                    // if route destination airport is not in relevant list
                    if (nextAirport == null)
                    {
                        // get route destination airport
                        nextAirport = new AirportRouteModel(airports.First(a => a.Id == route.DestinationAirportId));
                        // calculate estimated remaining tickets price based on route destination and final destination airport locations and calculated average price of ticket per km
                        nextAirport.EstimatedRemainingRoutesPrice = await _ticketService.EstimatedTicketsPriceBetweenTwoPoints(nextAirport.Location, destinationAirport.Location);
                        // add route destination to relevant airports
                        relevantAirports.Add(nextAirport);
                    }

                    // calculate new price to get from start airport to route destination airport
                    var newNextAirportPrice = currentAirport.AllRoutesPrice + route.Price;
                    // if old price is larger than new price 
                    if (nextAirport.AllRoutesPrice > newNextAirportPrice)
                    {
                        // update route destination airport previous airport
                        nextAirport.PreviousAirport = currentAirport;
                        // ticket price from previous airport
                        nextAirport.RoutePrice = route.Price;

                        // if next airport is final destination airport
                        if (nextAirport.Id == destinationAirport.Id)
                        {
                            destinationReached = true;
                            break;
                        }
                    }
                }

                // mark airport as processed in order not to process it again
                currentAirport.Processed = true;

                // filter relevant airports by removing final destination airport and processed ones
                // next airport for processing should be the one that has the lowest total estimated tickets price
                currentAirport = relevantAirports.Where(a => a.Id != destinationAirport.Id && !a.Processed).OrderBy(a => a.TotalEstimatedRoutesPrice).FirstOrDefault();
            }

            if (!destinationReached) throw new ApiNoFlightRouteFoundException();

            return await PrepareFlightRouteResult(sourceCity, destinationCity, sourceAirport, destinationAirport);
        }

        private async Task<FlightRouteResultModel> PrepareFlightRouteResult(CityModel sourceCity, CityModel destinationCity, AirportRouteModel sourceAirport, AirportRouteModel destinationAirport)
        {
            // prepare flight route result model
            FlightRouteResultModel result = new FlightRouteResultModel();
            result.SourceCity = $"{sourceCity.Name}, {sourceCity.Country}";
            result.DestinationCity = $"{destinationCity.Name}, {destinationCity.Country}";
            result.Price = destinationAirport.AllRoutesPrice;

            result.Route = new List<RouteSimpleResultModel>();

            // start from the last airport in order to recreate full route
            var currentAirport = destinationAirport;
            var currentCity = await _cityService.GetForAirport(currentAirport.Id);
            while (currentAirport.Id != sourceAirport.Id)
            {
                var previousCity = await _cityService.GetForAirport(currentAirport.PreviousAirport.Id);

                var route = new RouteSimpleResultModel();
                route.SourceCityId = previousCity.Id;
                route.DestinationCityId = currentCity.Id;
                route.Source = $"{previousCity.Airports.First(a => a.Id == currentAirport.PreviousAirport.Id).Name}, {previousCity.Name}, {previousCity.Country}";
                route.Destination = $"{currentCity.Airports.First(a => a.Id == currentAirport.Id).Name}, {currentCity.Name}, {currentCity.Country}";
                route.Price = currentAirport.RoutePrice;
                route.Length = GeoCalculator.GetDistance(currentAirport.PreviousAirport.Location, currentAirport.Location, 2, DistanceUnit.Kilometers);
                result.Route.Add(route);

                currentAirport = currentAirport.PreviousAirport;
                currentCity = previousCity;
            }
            result.Route.Reverse();

            // remove first or last route if they are transition within the same city
            var firstRoute = result.Route.First();
            if (firstRoute.SourceCityId == firstRoute.DestinationCityId)
            {
                result.Route.Remove(firstRoute);
            }
            var lastRoute = result.Route.Last();
            if (lastRoute.SourceCityId == lastRoute.DestinationCityId)
            {
                result.Route.Remove(lastRoute);
            }

            result.Length = result.Route.Sum(r => r.Length);

            return result;
        }
    }
}
