﻿using FlightAdvisor.Api.Lib.ServiceInterfaces;
using FlightAdvisor.Api.Database.Contexts;
using System.Threading.Tasks;
using FlightAdvisor.Api.Lib.Models;
using FlightAdvisor.Api.Lib.Helpers;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Collections.Generic;
using FlightAdvisor.Api.Database.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using FlightAdvisor.Common.Models;
using Microsoft.EntityFrameworkCore;

namespace FlightAdvisor.Api.Lib.Services
{
    public class AirlineService : IAirlineService
    {
        private readonly FlightAdvisorApiDbContext _flightAdvisorApiDbContext;
        private readonly ICacheService _cacheService;
        private readonly ILogger<AirportService> _logger;

        public AirlineService(FlightAdvisorApiDbContext flightAdvisorApiDbContext, ICacheService cacheService, ILogger<AirportService> logger)
        {
            _flightAdvisorApiDbContext = flightAdvisorApiDbContext;
            _cacheService = cacheService;
            _logger = logger;
        }

        public async Task ImportAirlines(IFormFile file)
        {
            await _flightAdvisorApiDbContext.Database.ExecuteSqlRawAsync("DELETE FROM Airlines");

            List<AirportCacheModel> airports = await _cacheService.GetAirports();
            List<int> airportIds = airports.Select(a => a.Id).ToList();

            List<Airline> existingAirlines = new List<Airline>();

            using (var reader = new StreamReader(file.OpenReadStream()))
            {
                while (reader.Peek() >= 0)
                {
                    try
                    {
                        Airline airline = AirlineHelper.FromString(await reader.ReadLineAsync(), airportIds);
                        if (airline != null && !existingAirlines.Any(a => a.Code == airline.Code))
                        {
                            existingAirlines.Add(airline);
                            _flightAdvisorApiDbContext.Airlines.Add(airline);
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex, ex.Message);
                    }
                }
            }
            await _flightAdvisorApiDbContext.SaveChangesAsync();
        }

        public async Task<ListResultModel<AirlineModel>> List(ListModel list)
        {
            var result = new ListResultModel<AirlineModel>();

            var allAirlinesDb = _flightAdvisorApiDbContext.Airlines.AsQueryable();
            var airlinesDb = allAirlinesDb;
            if (!string.IsNullOrEmpty(list.Filter))
            {
                airlinesDb = airlinesDb.Where(c => EF.Functions.Like(c.Code, $"%{list.Filter}%"));
            }

            result.Filtered = await airlinesDb.CountAsync();
            result.Total = await allAirlinesDb.CountAsync();

            if (list.Page != null && list.PageSize != null)
            {
                airlinesDb = airlinesDb.Skip((list.Page.Value - 1) * list.PageSize.Value);
            }

            if (list.PageSize != null)
            {
                airlinesDb = airlinesDb.Take(list.PageSize.Value);
            }

            result.Data = await airlinesDb.Select(a => AirlineHelper.DbToBusiness(a)).ToListAsync();

            return result;
        }
    }
}
