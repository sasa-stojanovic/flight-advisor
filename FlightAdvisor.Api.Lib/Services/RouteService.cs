﻿using FlightAdvisor.Api.Lib.ServiceInterfaces;
using FlightAdvisor.Api.Database.Contexts;
using System.Threading.Tasks;
using FlightAdvisor.Api.Lib.Models;
using FlightAdvisor.Api.Lib.Helpers;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Collections.Generic;
using FlightAdvisor.Api.Database.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using FlightAdvisor.Common.Models;
using Microsoft.EntityFrameworkCore;
using FlightAdvisor.Api.Lib.Configuration;
using Microsoft.Extensions.Options;

namespace FlightAdvisor.Api.Lib.Services
{
    public class RouteService : IRouteService
    {
        private readonly FlightAdvisorApiDbContext _flightAdvisorApiDbContext;
        private readonly IAirlineService _airlineService;
        private readonly ICacheService _cacheService;
        private readonly ITicketService _ticketService;
        private readonly ILogger<AirportService> _logger;

        public RouteService(FlightAdvisorApiDbContext flightAdvisorApiDbContext, IAirlineService airlineService, ICacheService cacheService, ITicketService ticketService, ILogger<AirportService> logger)
        {
            _flightAdvisorApiDbContext = flightAdvisorApiDbContext;
            _airlineService = airlineService;
            _cacheService = cacheService;
            _ticketService = ticketService;
            _logger = logger;
        }

        public async Task ImportRoutes(IFormFile file)
        {
            await _airlineService.ImportAirlines(file);

            await _flightAdvisorApiDbContext.Database.ExecuteSqlRawAsync("DELETE FROM Routes");

            List<AirlineModel> airlines = (await _airlineService.List(new ListModel())).Data;

            List<AirportCacheModel> airports = await _cacheService.GetAirports();
            List<int> airportIds = airports.Select(a => a.Id).ToList();

            using (var reader = new StreamReader(file.OpenReadStream()))
            {
                while (reader.Peek() >= 0)
                {
                    try
                    {
                        Route route = RouteHelper.FromString(await reader.ReadLineAsync(), airlines, airportIds);
                        if (route != null)
                        {
                            _flightAdvisorApiDbContext.Routes.Add(route);
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex, ex.Message);
                    }
                }
            }
            await _flightAdvisorApiDbContext.SaveChangesAsync();

            await RefreshCache();
        }

        public async Task RefreshCache()
        {
            // add standard routes
            List<RouteCacheModel> routes = _flightAdvisorApiDbContext.Routes.Select(r => new RouteCacheModel() { SourceAirportId = r.SourceAirportId, DestinationAirportId = r.DestinationAirportId, Price = r.Price }).ToList();

            // add free routes between same city
            var cities = _flightAdvisorApiDbContext.Cities.Include(c => c.Airports).Where(c => c.Airports.Count() > 1).Select(c => new { Id = c.Id, Airports = c.Airports.Select(a => a.Id) }).ToList();
            foreach (var city in cities)
            {
                foreach (var sourceAirportId in city.Airports)
                {
                    foreach (var destinationAirportId in city.Airports)
                    {
                        if (sourceAirportId != destinationAirportId)
                        {
                            routes.Add(new RouteCacheModel() { SourceAirportId = sourceAirportId, DestinationAirportId = destinationAirportId, Price = 0 });
                        }
                    }
                }
            }

            await _cacheService.SaveRoutes(routes);
            await _ticketService.RecalculateEstimatedTicketsPricePerKilometer();
        }
    }
}
