﻿using Microsoft.EntityFrameworkCore;
using FlightAdvisor.Api.Lib.ServiceInterfaces;
using FlightAdvisor.Api.Database.Contexts;
using System.Threading.Tasks;
using FlightAdvisor.Api.Lib.Models;
using System.Linq;
using FlightAdvisor.Common.Exceptions.General;
using FlightAdvisor.Api.Lib.Helpers;
using FlightAdvisor.Common.Models;
using System;
using FlightAdvisor.Api.Database.Models;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using FlightAdvisor.Api.Lib.Enums;
using System.Linq.Expressions;
using FlightAdvisor.Common.Enums;

namespace FlightAdvisor.Api.Lib.Services
{
    public class CityService : ICityService
    {
        private readonly FlightAdvisorApiDbContext _flightAdvisorApiDbContext;
        private readonly ILogger<CityService> _logger;

        public CityService(FlightAdvisorApiDbContext flightAdvisorApiDbContext, ILogger<CityService> logger)
        {
            _flightAdvisorApiDbContext = flightAdvisorApiDbContext;
            _logger = logger;
        }

        public async Task<CityModel> Get(int id)
        {
            var cityDb = await _flightAdvisorApiDbContext.Cities.Include(c => c.Airports).FirstOrDefaultAsync(c => c.Id == id);
            if (cityDb == null) throw new EntryNotFoundException("city", id);
            return CityHelper.DbToBusiness(cityDb);
        }

        public async Task<CityModel> GetForAirport(int airportId)
        {
            var cityDb = await _flightAdvisorApiDbContext.Cities.Include(c => c.Airports).FirstOrDefaultAsync(c => c.Airports.Any(a => a.Id == airportId));
            if (cityDb == null) throw new EntryNotFoundException();
            return CityHelper.DbToBusiness(cityDb);
        }

        public async Task<int> Create(CityModel city)
        {
            var cityDb = CityHelper.BusinessToDb(city);
            _flightAdvisorApiDbContext.Cities.Add(cityDb);
            await _flightAdvisorApiDbContext.SaveChangesAsync();
            return cityDb.Id;
        }

        public async Task<ListResultModel<CityModel>> List(ListCitiesModel<CitySortColumn> list)
        {
            var result = new ListResultModel<CityModel>();

            IQueryable<City> allCitiesDb;
            if (list.Comments == null)
            {
                allCitiesDb = _flightAdvisorApiDbContext.Cities.Include(c => c.Comments).AsQueryable();
            }
            else
            {
                allCitiesDb = _flightAdvisorApiDbContext.Cities.AsQueryable();
            }

            var citiesDb = allCitiesDb;
            if (!string.IsNullOrEmpty(list.Filter))
            {
                citiesDb = citiesDb.Where(c => EF.Functions.Like(c.Name, $"%{list.Filter}%"));
            }

            result.Filtered = await citiesDb.CountAsync();
            result.Total = await allCitiesDb.CountAsync();

            Expression<Func<City, object>> orderBy = null;

            switch (list.SortColumn)
            {
                case CitySortColumn.Name:
                    orderBy = c => c.Name;
                    break;
                case CitySortColumn.Country:
                    orderBy = c => c.Country;
                    break;
            }

            citiesDb = list.SortDirection == SortDirection.Asc ? citiesDb.OrderBy(orderBy) : citiesDb.OrderByDescending(orderBy);

            if (list.Page != null && list.PageSize != null)
            {
                citiesDb = citiesDb.Skip((list.Page.Value - 1) * list.PageSize.Value);
            }

            if (list.PageSize != null)
            {
                citiesDb = citiesDb.Take(list.PageSize.Value);
            }

            if (list.Comments == null)
            {
                result.Data = await citiesDb.Select(c => CityHelper.DbToBusiness(c)).ToListAsync();
            }
            else
            {
                //result.Data = (await citiesDb.Select(c => new { c, Comments = c.Comments.Take(list.Comments.Value).ToList() }).ToListAsync())
                //    .Select(x => { x.c.Comments = x.Comments; return CityHelper.DbToBusiness(x.c); }).ToList();

                var cities = await citiesDb.ToListAsync();
                string sql;
                if (cities.Count() != result.Total)
                {
                    sql = $"SELECT * FROM (SELECT *, ROW_NUMBER() OVER(PARTITION BY CityId ORDER BY CreatedDate DESC) as No FROM Comments) C WHERE C.No <= {list.Comments.Value} AND C.CityId IN ({string.Join(',', cities.Select(c => c.Id))})";
                }
                else
                {
                    sql = $"SELECT * FROM (SELECT *, ROW_NUMBER() OVER(PARTITION BY CityId ORDER BY CreatedDate DESC) as No FROM Comments) C WHERE C.No <= {list.Comments.Value}";
                }
                List<Comment> comments = await _flightAdvisorApiDbContext.Comments.FromSqlRaw(sql).ToListAsync();

                foreach (var city in cities)
                {
                    city.Comments = comments.Where(c => c.CityId == city.Id).ToList();
                }
                result.Data = cities.Select(c => CityHelper.DbToBusiness(c)).ToList();
            }

            return result;
        }

        public async Task ImportCities(IFormFile file)
        {
            await _flightAdvisorApiDbContext.Database.ExecuteSqlRawAsync("DELETE FROM Cities");

            List<City> existingCities = new List<City>();

            using (var reader = new StreamReader(file.OpenReadStream()))
            {
                while (reader.Peek() >= 0)
                {
                    try
                    {
                        City city = CityHelper.FromString(await reader.ReadLineAsync());
                        if (city != null && !string.IsNullOrEmpty(city.Name) && !string.IsNullOrEmpty(city.Country) && !existingCities.Any(c => c.Name == city.Name && c.Country == city.Country))
                        {
                            existingCities.Add(city);
                            _flightAdvisorApiDbContext.Cities.Add(city);
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex, ex.Message);
                    }
                }
            }
            await _flightAdvisorApiDbContext.SaveChangesAsync();
        }
    }
}
