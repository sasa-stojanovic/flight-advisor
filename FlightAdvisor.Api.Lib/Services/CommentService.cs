﻿using Microsoft.EntityFrameworkCore;
using FlightAdvisor.Api.Lib.ServiceInterfaces;
using FlightAdvisor.Api.Database.Contexts;
using System.Threading.Tasks;
using FlightAdvisor.Api.Lib.Models;
using System.Linq;
using FlightAdvisor.Common.Exceptions.General;
using FlightAdvisor.Api.Lib.Helpers;
using FlightAdvisor.Common.Models;
using System;
using FlightAdvisor.Api.Database.Models;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using FlightAdvisor.Api.Lib.Enums;
using System.Linq.Expressions;
using FlightAdvisor.Common.Enums;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;

namespace FlightAdvisor.Api.Lib.Services
{
    public class CommentService : ICommentService
    {
        private readonly FlightAdvisorApiDbContext _flightAdvisorApiDbContext;

        public CommentService(FlightAdvisorApiDbContext flightAdvisorApiDbContext)
        {
            _flightAdvisorApiDbContext = flightAdvisorApiDbContext;
        }

        public async Task<int> Create(CommentModel comment, ClaimsPrincipal actionUser)
        {
            comment.UserId = Convert.ToInt32(actionUser.FindFirstValue(ClaimTypes.NameIdentifier));
            comment.User = $"{actionUser.FindFirstValue(JwtRegisteredClaimNames.GivenName)} {actionUser.FindFirstValue(JwtRegisteredClaimNames.FamilyName)}";
            comment.CreatedDate = DateTime.UtcNow;
            comment.ModifyDate = DateTime.UtcNow;
            var commentDb = CommentHelper.BusinessToDb(comment);

            _flightAdvisorApiDbContext.Comments.Add(commentDb);
            await _flightAdvisorApiDbContext.SaveChangesAsync();
            return commentDb.Id;
        }

        public async Task Edit(CommentModel comment, ClaimsPrincipal actionUser)
        {
            var commentDb = _flightAdvisorApiDbContext.Comments.FirstOrDefault(c => c.Id == comment.Id);
            if (commentDb == null) throw new EntryNotFoundException("comment", comment.Id);
            if (commentDb.UserId != Convert.ToInt32(actionUser.FindFirstValue(ClaimTypes.NameIdentifier))) throw new NoPermissionException();

            commentDb.Text = comment.Text;
            commentDb.ModifyDate = DateTime.UtcNow;

            await _flightAdvisorApiDbContext.SaveChangesAsync();
        }

        public async Task Delete(DeleteModel delete, ClaimsPrincipal actionUser)
        {
            var commentDb = _flightAdvisorApiDbContext.Comments.FirstOrDefault(c => c.Id == delete.Id);
            if (commentDb == null) throw new EntryNotFoundException("comment", delete.Id);
            if (commentDb.UserId != Convert.ToInt32(actionUser.FindFirstValue(ClaimTypes.NameIdentifier))) throw new NoPermissionException();

            _flightAdvisorApiDbContext.Comments.Remove(commentDb);
            await _flightAdvisorApiDbContext.SaveChangesAsync();
        }
    }
}
