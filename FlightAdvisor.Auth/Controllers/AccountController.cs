﻿using FlightAdvisor.Auth.Lib.Enums;
using FlightAdvisor.Auth.Lib.Models;
using FlightAdvisor.Auth.Lib.ServiceInterfaces;
using FlightAdvisor.Auth.Models;
using FlightAdvisor.Common.Constants;
using FlightAdvisor.Common.Exceptions.General;
using FlightAdvisor.Common.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace FlightAdvisor.Auth.Controllers
{
    public class AccountController : ControllerBase
    {
        private readonly IAccountService _accountService;
        private readonly IPermissionService _permissionService;
        private readonly ILogger<AccountController> _logger;

        public AccountController(IAccountService accountService, IPermissionService permissionService, ILogger<AccountController> logger)
        {
            _accountService = accountService;
            _permissionService = permissionService;
            _logger = logger;
        }

        [HttpPost]
        public async Task<AuthResultModel<TokenModel>> Login([FromBody] LoginModel login)
        {
            try
            {
                if (!ModelState.IsValid) throw new InvalidModelDataException(ModelState);

                return new AuthResultModel<TokenModel>(await _accountService.Login(login));

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return new AuthResultModel<TokenModel>(ex);
            }
        }

        [HttpDelete]
        public async Task<AuthResultModel> Logout([FromBody] LogoutModel logout)
        {
            try
            {
                if (!ModelState.IsValid) throw new InvalidModelDataException(ModelState);

                await _accountService.Logout(logout);
                return new AuthResultModel();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return new AuthResultModel(ex);
            }
        }

        [HttpPost]
        public async Task<AuthResultModel<TokenModel>> Register([FromBody] UserModel user)
        {
            try
            {
                if (!ModelState.IsValid) throw new InvalidModelDataException(ModelState);

                return new AuthResultModel<TokenModel>(await _accountService.Register(user));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return new AuthResultModel<TokenModel>(ex);
            }
        }

        [HttpPut]
        [Authorize(Roles = RoleConstants.Admin + "," + RoleConstants.User)]
        public async Task<AuthResultModel> Edit([FromBody] UserModel user)
        {
            try
            {
                if (!ModelState.IsValid) throw new InvalidModelDataException(ModelState);

                _permissionService.CheckUserEdit(User, user);

                await _accountService.Edit(user);
                return new AuthResultModel();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return new AuthResultModel(ex);
            }
        }

        [HttpPatch]
        [Authorize(Roles = RoleConstants.Admin)]
        public async Task<AuthResultModel> SetStatus([FromBody] UserStatusModel userStatus)
        {
            try
            {
                if (!ModelState.IsValid) throw new InvalidModelDataException(ModelState);

                await _accountService.SetStatus(userStatus);
                return new AuthResultModel();

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return new AuthResultModel(ex);
            }
        }

        [HttpGet]
        [Authorize(Roles = RoleConstants.Admin + "," + RoleConstants.User)]
        public async Task<AuthResultModel<UserModel>> Get([FromQuery] GetModel get)
        {
            try
            {
                if (!ModelState.IsValid) throw new InvalidModelDataException(ModelState);

                _permissionService.CheckUserGet(User, get.Id);

                return new AuthResultModel<UserModel>(await _accountService.Get(get));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return new AuthResultModel<UserModel>(ex);
            }
        }

        [HttpGet]
        [Authorize(Roles = RoleConstants.Admin)]
        public async Task<AuthResultModel<ListResultModel<UserModel>>> List([FromQuery] ListModel<UserSortColumn> list)
        {
            try
            {
                if (!ModelState.IsValid) throw new InvalidModelDataException(ModelState);
                
                return new AuthResultModel<ListResultModel<UserModel>>(await _accountService.List(list));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return new AuthResultModel<ListResultModel<UserModel>>(ex);
            }
        }

        [HttpDelete]
        [Authorize(Roles = RoleConstants.Admin + "," + RoleConstants.User)]
        public async Task<AuthResultModel> Delete([FromBody] DeleteModel delete)
        {
            try
            {
                if (!ModelState.IsValid) throw new InvalidModelDataException(ModelState);

                _permissionService.CheckUserDelete(User, delete.Id);

                await _accountService.Delete(delete);
                return new AuthResultModel();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return new AuthResultModel(ex);
            }
        }
    }
}
