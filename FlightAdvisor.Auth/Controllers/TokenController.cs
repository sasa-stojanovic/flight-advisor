﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using FlightAdvisor.Auth.Lib.ServiceInterfaces;
using FlightAdvisor.Auth.Models;
using FlightAdvisor.Common.Exceptions.General;
using Microsoft.Extensions.Logging;
using FlightAdvisor.Auth.Lib.Models;
using FlightAdvisor.Common.Constants;
using Microsoft.AspNetCore.Authorization;

namespace FlightAdvisor.Auth.Controllers
{
    public class TokenController : Controller
    {
        private readonly ITokenService _tokenService;
        private readonly ILogger<TokenController> _logger;

        public TokenController(ITokenService tokenService, ILogger<TokenController> logger)
        {
            _tokenService = tokenService;
            _logger = logger;
        }

        [HttpPost]
        public async Task<AuthResultModel<TokenModel>> Refresh([FromBody]TokenModel token)
        {
            try
            {
                if (!ModelState.IsValid) throw new InvalidModelDataException(ModelState);
                
                return new AuthResultModel<TokenModel>(await _tokenService.Refresh(token));

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return new AuthResultModel<TokenModel>(ex);
            }
        }

        [HttpDelete]
        [Authorize(Roles = RoleConstants.Admin)]
        public async Task<AuthResultModel> Revoke()
        {
            try
            {
                if (!ModelState.IsValid) throw new InvalidModelDataException(ModelState);

                await _tokenService.Revoke(User, Request);
                return new AuthResultModel();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return new AuthResultModel(ex);
            }
        }
    }
}
