﻿using System;
using FlightAdvisor.Auth.Helpers;
using FlightAdvisor.Common.Models;

namespace FlightAdvisor.Auth.Models
{
    public class AuthResultModel : ResultModel
    {
        public AuthResultModel() : base()
        {

        }

        public AuthResultModel(Exception e) : base(e)
        {
            ErrorType = e.GetErrorType();
        }
    }

    public class AuthResultModel<T> : ResultModel<T>
    {
        public AuthResultModel() : base()
        {

        }

        public AuthResultModel(Exception e) : base(e)
        {
            ErrorType = e.GetErrorType();
        }

        public AuthResultModel(T data) : base(data)
        {

        }
    }
}
