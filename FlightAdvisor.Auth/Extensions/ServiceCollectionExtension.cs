﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using System;
using FlightAdvisor.Auth.Lib.Extensions;
using FlightAdvisor.Auth.Lib.Configuration;
using FlightAdvisor.Auth.Lib.Constants;
using System.Net.Mime;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using FlightAdvisor.Auth.Models;
using FlightAdvisor.Common.Exceptions.Auth;
using Newtonsoft.Json.Serialization;
using System.Threading.Tasks;

namespace FlightAdvisor.Auth.Extensions
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddFlightAdvisorAuthServices(this IServiceCollection services, IConfiguration configuration)
        {
            var authSettingsSection = configuration.GetSection(ConfigConstants.AuthSettings);
            services.Configure<AuthSettings>(authSettingsSection);
            var authSettings = authSettingsSection.Get<AuthSettings>();

            // ===== Add Jwt Authentication ========
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear(); // => remove default claims
            services
                .AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(cfg =>
                {
                    cfg.RequireHttpsMetadata = false;
                    cfg.SaveToken = true;
                    cfg.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidIssuer = authSettings.Token.Issuer,
                        ValidAudience = authSettings.Token.Issuer,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(authSettings.Token.Key)),
                        ClockSkew = TimeSpan.Zero // remove delay of token when expire
                    };
                    cfg.Events = new JwtBearerEvents
                    {
                        OnChallenge = context =>
                        {
                            context.Response.ContentType = MediaTypeNames.Application.Json;
                            if (context.AuthenticateFailure != null && context.AuthenticateFailure.GetType() == typeof(SecurityTokenExpiredException))
                            {
                                context.Response.WriteAsync(JsonConvert.SerializeObject(new AuthResultModel(new AuthExpiredTokenException()), new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() })).Wait();
                            }
                            else
                            {
                                context.Response.WriteAsync(JsonConvert.SerializeObject(new AuthResultModel(new AuthInvalidTokenException()), new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() })).Wait();
                            }

                            context.HandleResponse();
                            return Task.CompletedTask;
                        }
                    };
                });

            services.AddFlightAdvisorAuthLibServices(configuration);

            return services;
        }
    }
}
