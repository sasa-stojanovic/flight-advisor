﻿using System;
using FlightAdvisor.Common.Enums;
using FlightAdvisor.Common.Exceptions.Auth;
using FlightAdvisor.Common.Exceptions.General;
using FlightAdvisor.Common.Models;

namespace FlightAdvisor.Auth.Helpers
{
    public static class ExceptionHelper
    {
        public static ErrorType GetErrorType(this Exception e)
        {
            switch (e)
            {
                case UnknownErrorException ex:
                    return ErrorType.UnknownError;
                case NoPermissionException ex:
                    return ErrorType.NoPermission;
                case InvalidModelDataException ex:
                    return ErrorType.InvalidModelDataError;
                case ChangesNotSavedException ex:
                    return ErrorType.NoChangesSavedError;
                case EntryNotFoundException ex:
                    return ErrorType.EntryNotFoundError;
                case AuthUsernameAlreadyRegisteredException ex:
                    return ErrorType.UsernameAlreadyRegisteredError;
                case AuthInvalidUserException ex:
                    return ErrorType.InvalidUserError;
                case AuthInvalidPasswordException ex:
                    return ErrorType.InvalidPasswordError;
                case AuthInvalidTokenException ex:
                    return ErrorType.InvalidTokenError;
                case AuthExpiredTokenException ex:
                    return ErrorType.ExpiredTokenError;
                case AuthInvalidLoginAttemptException ex:
                    return ErrorType.InvalidLoginAttemptError;
                case AuthInactiveUserException ex:
                    return ErrorType.InactiveUserError;
            }

            return ErrorType.UnknownError;
        }

        public static void ThrowException(this ResultModel apiResult)
        {
            switch (apiResult.ErrorType)
            {
                case ErrorType.UnknownError:
                    throw new UnknownErrorException(apiResult.Message);
                case ErrorType.InvalidModelDataError:
                    throw new InvalidModelDataException(apiResult.Message);
                case ErrorType.NoPermission:
                    throw new NoPermissionException(apiResult.Message);
                case ErrorType.NoChangesSavedError:
                    throw new ChangesNotSavedException(apiResult.Message);
                case ErrorType.EntryNotFoundError:
                    throw new EntryNotFoundException(apiResult.Message);
                case ErrorType.UsernameAlreadyRegisteredError:
                    throw new AuthUsernameAlreadyRegisteredException(apiResult.Message);
                case ErrorType.InvalidUserError:
                    throw new AuthInvalidUserException(apiResult.Message);
                case ErrorType.InvalidPasswordError:
                    throw new AuthInvalidPasswordException(apiResult.Message);
                case ErrorType.InvalidTokenError:
                    throw new AuthInvalidTokenException(apiResult.Message);
                case ErrorType.ExpiredTokenError:
                    throw new AuthExpiredTokenException(apiResult.Message);
                case ErrorType.InvalidLoginAttemptError:
                    throw new AuthInvalidLoginAttemptException(apiResult.Message);
                case ErrorType.InactiveUserError:
                    throw new AuthInactiveUserException(apiResult.Message);
                default:
                    throw new UnknownErrorException(apiResult.Message);
            }
        }
    }
}
