﻿using Microsoft.Extensions.DependencyInjection;
using FlightAdvisor.Auth.Database.Contexts;
using Microsoft.EntityFrameworkCore;

namespace FlightAdvisor.Auth.Lib.Extensions
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddFlightAdvisorAuthDatabaseServices(this IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options => options.UseSqlite("Data Source=FlightAdvisor.Auth.db"));

            return services;
        }
    }
}
