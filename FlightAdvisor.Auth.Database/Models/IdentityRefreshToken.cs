﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FlightAdvisor.Auth.Database.Models
{
    [Table("AspNetUserRefreshTokens")]
    public class IdentityRefreshToken
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required]
        public byte[] Token { get; set; }

        public DateTime Issued { get; set; }

        public DateTime Expires { get; set; }

        [Required]
        public virtual IdentityAppUser User { get; set; }
    }
}
