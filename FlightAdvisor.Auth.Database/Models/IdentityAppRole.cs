﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace FlightAdvisor.Auth.Database.Models
{
    public class IdentityAppRole : IdentityRole<int>
    {
        public virtual ICollection<IdentityAppUserRole> UserRoles { get; set; }

        public IdentityAppRole() : base()
        {
            UserRoles = new HashSet<IdentityAppUserRole>();
        }

        public IdentityAppRole(string roleName) : base(roleName)
        {
            UserRoles = new HashSet<IdentityAppUserRole>();
        }
    }
}
