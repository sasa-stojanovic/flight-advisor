﻿using Microsoft.AspNetCore.Identity;
namespace FlightAdvisor.Auth.Database.Models
{
    public class IdentityAppUserRole : IdentityUserRole<int>
    {
        public virtual IdentityAppUser User { get; set; }
        public virtual IdentityAppRole Role { get; set; }
    }
}
