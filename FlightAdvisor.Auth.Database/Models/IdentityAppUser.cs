﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FlightAdvisor.Auth.Database.Models
{
    public class IdentityAppUser : IdentityUser<int>
    {
        [Required(AllowEmptyStrings = false)]
        [StringLength(50)]
        public string FirstName { get; set; }

        [Required(AllowEmptyStrings = false)]
        [StringLength(50)]
        public string LastName { get; set; }

        public bool Active { get; set; }

        public virtual ICollection<IdentityRefreshToken> RefreshTokens { get; set; }

        public virtual ICollection<IdentityAppUserRole> UserRoles { get; set; }

        public IdentityAppUser() : base()
        {
            RefreshTokens = new HashSet<IdentityRefreshToken>();
            UserRoles = new HashSet<IdentityAppUserRole>();
        }
    }
}
