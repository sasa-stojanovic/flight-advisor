﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlightAdvisor.Api.Database.Models
{
    public class Airport
    {
        public Airport()
        {
            SourceRoutes = new HashSet<Route>();
            DestinationRoutes = new HashSet<Route>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int CityId { get; set; }
        public string IATA { get; set; }
        public string ICAO { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public decimal Altitude { get; set; }
        public decimal? Timezone { get; set; }
        public char? DST { get; set; }
        public string Tz { get; set; }
        public string Type { get; set; }
        public string Source { get; set; }

        public virtual City City { get; set; }
        public virtual ICollection<Route> SourceRoutes { get; set; }
        public virtual ICollection<Route> DestinationRoutes { get; set; }

    }
}
