﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlightAdvisor.Api.Database.Models
{
    public class City
    {
        public City()
        {
            Airports = new HashSet<Airport>();
            Comments = new HashSet<Comment>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Airport> Airports { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
    }
}
