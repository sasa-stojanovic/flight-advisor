﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlightAdvisor.Api.Database.Models
{
    public class Airline
    {
        public Airline()
        {
            Routes = new HashSet<Route>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public virtual ICollection<Route> Routes { get; set; }
    }
}
