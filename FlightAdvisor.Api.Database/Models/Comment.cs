﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlightAdvisor.Api.Database.Models
{
    public class Comment
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public int CityId { get; set; }
        public int UserId { get; set; }
        public string User { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifyDate { get; set; }

        public virtual City City { get; set; }
    }
}