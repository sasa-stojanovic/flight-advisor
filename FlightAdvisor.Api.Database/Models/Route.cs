﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlightAdvisor.Api.Database.Models
{
    public class Route
    {
        public int Id { get; set; }
        public int AirlineId { get; set; }
        public int SourceAirportId { get; set; }
        public int DestinationAirportId { get; set; }
        public bool Codeshare { get; set; }
        public int Stops { get; set; }
        public string Equipment { get; set; }
        public decimal Price { get; set; }

        public virtual Airline Airline { get; set; }
        public virtual Airport SourceAirport { get; set; }
        public virtual Airport DestinationAirport { get; set; }
    }
}
