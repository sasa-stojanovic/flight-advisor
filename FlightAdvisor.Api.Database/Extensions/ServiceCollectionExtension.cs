﻿using Microsoft.Extensions.DependencyInjection;
using FlightAdvisor.Api.Database.Contexts;
using Microsoft.EntityFrameworkCore;

namespace FlightAdvisor.Api.Database.Extensions
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddFlightAdvisorApiDatabaseServices(this IServiceCollection services)
        {
            services.AddDbContext<FlightAdvisorApiDbContext>(options => options.UseSqlite("Data Source=FlightAdvisor.Api.db"));

            return services;
        }
    }
}
