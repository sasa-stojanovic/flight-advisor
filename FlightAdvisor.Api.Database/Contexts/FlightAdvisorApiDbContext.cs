﻿using FlightAdvisor.Api.Database.Models;
using Microsoft.EntityFrameworkCore;

namespace FlightAdvisor.Api.Database.Contexts
{
    public class FlightAdvisorApiDbContext : DbContext
    {
        public FlightAdvisorApiDbContext(DbContextOptions<FlightAdvisorApiDbContext> options) : base(options)
        {
        }

        public DbSet<City> Cities { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Airline> Airlines { get; set; }
        public DbSet<Airport> Airports { get; set; }
        public DbSet<Route> Routes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<City>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Name).IsRequired().HasMaxLength(200);
                entity.Property(e => e.Country).IsRequired().HasMaxLength(200);
                entity.Property(e => e.Description).IsRequired().HasMaxLength(2000);
            });

            modelBuilder.Entity<Comment>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Text).IsRequired().HasMaxLength(2000);
                entity.Property(e => e.CityId).IsRequired();
                entity.Property(e => e.UserId).IsRequired();
                entity.Property(e => e.User).IsRequired();
                entity.Property(e => e.CreatedDate).IsRequired();
                entity.Property(e => e.ModifyDate).IsRequired();

                entity.HasIndex(e => e.CityId);

                entity.HasOne(e => e.City)
                    .WithMany(e => e.Comments)
                    .HasForeignKey(e => e.CityId)
                    .HasConstraintName("FK_Comments_Cities");
            });

            modelBuilder.Entity<Airport>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Name).IsRequired().HasMaxLength(200);
                entity.Property(e => e.CityId).IsRequired();
                entity.Property(e => e.IATA).HasMaxLength(3);
                entity.Property(e => e.ICAO).HasMaxLength(3);

                entity.HasIndex(e => e.CityId);

                entity.HasOne(e => e.City)
                    .WithMany(e => e.Airports)
                    .HasForeignKey(e => e.CityId)
                    .HasConstraintName("FK_Airports_Cities");
            });

            modelBuilder.Entity<Airline>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Code).IsRequired().HasMaxLength(3);
            });

            modelBuilder.Entity<Route>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.AirlineId).IsRequired();
                entity.Property(e => e.SourceAirportId).IsRequired();
                entity.Property(e => e.DestinationAirportId).IsRequired();
                entity.Property(e => e.Price).IsRequired();

                entity.HasOne(e => e.Airline)
                    .WithMany(e => e.Routes)
                    .HasForeignKey(e => e.AirlineId)
                    .HasConstraintName("FK_Routes_Airlines");

                entity.HasOne(e => e.SourceAirport)
                    .WithMany(e => e.SourceRoutes)
                    .HasForeignKey(e => e.SourceAirportId)
                    .HasConstraintName("FK_Routes_SourceAirports");

                entity.HasOne(e => e.DestinationAirport)
                    .WithMany(e => e.DestinationRoutes)
                    .HasForeignKey(e => e.DestinationAirportId)
                    .HasConstraintName("FK_Routes_DestinationAirports");
            });
        }
    }
}
