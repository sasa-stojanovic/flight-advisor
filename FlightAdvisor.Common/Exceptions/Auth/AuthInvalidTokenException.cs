﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FlightAdvisor.Common.Exceptions.Auth
{
    public class AuthInvalidTokenException : Exception
    {
        public AuthInvalidTokenException()
        {
        }

        public AuthInvalidTokenException(string message) : base(message)
        {
        }

        public AuthInvalidTokenException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public AuthInvalidTokenException(IEnumerable<IdentityError> errors) : base($"Errors: '{string.Join(" ", errors.Select(e => e.Description))}'")
        {
        }
    }
}
