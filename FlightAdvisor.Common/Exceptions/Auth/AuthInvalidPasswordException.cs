﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FlightAdvisor.Common.Exceptions.Auth
{
    public class AuthInvalidPasswordException : Exception
    {
        public AuthInvalidPasswordException()
        {
        }

        public AuthInvalidPasswordException(string message) : base(message)
        {
        }

        public AuthInvalidPasswordException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public AuthInvalidPasswordException(IEnumerable<IdentityError> errors): base($"Errors: '{string.Join(" ", errors.Select(e => e.Description))}'")
        {

        }
    }
}
