﻿using System;

namespace FlightAdvisor.Common.Exceptions.Auth
{
    public class AuthExpiredTokenException : Exception
    {
        public AuthExpiredTokenException()
        {
        }

        public AuthExpiredTokenException(string message) : base(message)
        {
        }

        public AuthExpiredTokenException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
