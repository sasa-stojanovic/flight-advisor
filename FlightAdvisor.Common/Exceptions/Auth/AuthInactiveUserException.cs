﻿using System;

namespace FlightAdvisor.Common.Exceptions.Auth
{
    public class AuthInactiveUserException : Exception
    {
        public AuthInactiveUserException()
        {
        }

        public AuthInactiveUserException(string message) : base(message)
        {
        }

        public AuthInactiveUserException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
