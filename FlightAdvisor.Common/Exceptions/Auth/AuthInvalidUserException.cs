﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FlightAdvisor.Common.Exceptions.Auth
{
    public class AuthInvalidUserException : Exception
    {
        public AuthInvalidUserException()
        {
        }

        public AuthInvalidUserException(string message) : base(message)
        {
        }

        public AuthInvalidUserException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public AuthInvalidUserException(IEnumerable<IdentityError> errors): base($"Errors: '{string.Join(" ", errors.Select(e => e.Description))}'")
        {

        }
    }
}
