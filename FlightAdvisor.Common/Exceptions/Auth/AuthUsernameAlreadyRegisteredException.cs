﻿using System;

namespace FlightAdvisor.Common.Exceptions.Auth
{
    public class AuthUsernameAlreadyRegisteredException : Exception
    {
        public AuthUsernameAlreadyRegisteredException()
        {
        }

        public AuthUsernameAlreadyRegisteredException(string message) : base(message)
        {
        }

        public AuthUsernameAlreadyRegisteredException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
