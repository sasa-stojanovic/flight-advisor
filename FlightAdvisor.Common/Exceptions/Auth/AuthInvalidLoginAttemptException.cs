﻿using System;

namespace FlightAdvisor.Common.Exceptions.Auth
{
    public class AuthInvalidLoginAttemptException : Exception
    {
        public AuthInvalidLoginAttemptException()
        {
        }

        public AuthInvalidLoginAttemptException(string message) : base(message)
        {
        }

        public AuthInvalidLoginAttemptException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
