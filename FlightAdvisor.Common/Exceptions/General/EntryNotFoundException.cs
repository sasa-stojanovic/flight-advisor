﻿using System;

namespace FlightAdvisor.Common.Exceptions.General
{
    public class EntryNotFoundException : Exception
    {
        public EntryNotFoundException()
        {
        }

        public EntryNotFoundException(string message) : base(message)
        {
        }

        public EntryNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public EntryNotFoundException(string entryName, int id) : base($"Entry '{entryName}' with id '{id}' not found")
        {
        }
    }
}
