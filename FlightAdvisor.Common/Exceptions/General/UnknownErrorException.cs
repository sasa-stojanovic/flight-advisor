﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FlightAdvisor.Common.Exceptions.General
{
    public class UnknownErrorException : Exception
    {
        public UnknownErrorException()
        {
        }

        public UnknownErrorException(string message) : base(message)
        {
        }

        public UnknownErrorException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public UnknownErrorException(IEnumerable<IdentityError> errors) : base($"Errors: '{string.Join(" ", errors.Select(e => e.Description))}'")
        {
        }
    }
}
