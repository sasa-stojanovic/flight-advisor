﻿using System;

namespace FlightAdvisor.Common.Exceptions.General
{
    public class ChangesNotSavedException : Exception
    {
        public ChangesNotSavedException()
        {
        }

        public ChangesNotSavedException(string message) : base(message)
        {
        }

        public ChangesNotSavedException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
