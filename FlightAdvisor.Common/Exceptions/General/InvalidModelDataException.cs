﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Linq;

namespace FlightAdvisor.Common.Exceptions.General
{
    public class InvalidModelDataException : Exception
    {
        public InvalidModelDataException()
        {
        }

        public InvalidModelDataException(string message) : base(message)
        {
        }

        public InvalidModelDataException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public InvalidModelDataException(ModelStateDictionary modelState) : base("Invalid model data: " + string.Join(" ", modelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage)))
        {
        }
    }
}
