﻿using FlightAdvisor.Common.Enums;
using System;

namespace FlightAdvisor.Common.Models
{
    public abstract class ResultModel
    {
        public ErrorType ErrorType { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }

        protected ResultModel()
        {
            Success = true;
            Message = null;
            ErrorType = ErrorType.NoError;
        }

        protected ResultModel(Exception e)
        {
            Success = false;
            Message = e.Message;
        }
    }

    public abstract class ResultModel<T> : ResultModel
    {
        public T Data { get; set; }

        protected ResultModel() : base()
        {

        }

        protected ResultModel(T data) : base()
        {
            Data = data;
        }

        protected ResultModel(Exception e) : base(e)
        {

        }
    }
}
