﻿using System;
using FlightAdvisor.Common.Enums;

namespace FlightAdvisor.Common.Models
{
    public class ListModel
    {
        public string Filter { get; set; }
        public int? PageSize { get; set; }
        public int? Page { get; set; }
        public SortDirection SortDirection { get; set; } = SortDirection.Asc;
        public Guid? ParentId { get; set; }
    }

    public class ListModel<T> : ListModel
    {
        public T SortColumn { get; set; }
    }
}
