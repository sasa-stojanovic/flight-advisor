﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FlightAdvisor.Common.Models
{
    public class GetModel
    {
        [Required]
        public int Id { get; set; }
    }
}
