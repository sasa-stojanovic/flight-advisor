﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FlightAdvisor.Common.Models
{
    public class DeleteModel
    {
        [Required]
        public int Id { get; set; }
    }
}
