﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlightAdvisor.Common.Models
{
    public class ListResultModel<T>
    {
        public List<T> Data { get; set; }
        public int Filtered { get; set; }
        public int Total { get; set; }
    }
}
