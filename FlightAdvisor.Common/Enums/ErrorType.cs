﻿namespace FlightAdvisor.Common.Enums
{
    public enum ErrorType
    {
        NoError = 0,
        UnknownError = 100,
        NoPermission = 101,
        InvalidModelDataError = 102,
        NoChangesSavedError = 103,
        EntryNotFoundError = 104,
        UsernameAlreadyRegisteredError = 200,
        InvalidUserError = 201,
        InvalidPasswordError = 202,
        InvalidTokenError = 203,
        ExpiredTokenError = 204,
        InvalidLoginAttemptError = 205,
        InactiveUserError = 206,
        NoFlightRouteFoundError = 300
    }
}
