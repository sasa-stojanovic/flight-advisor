﻿namespace FlightAdvisor.Common.Enums
{
    public enum SortDirection
    {
        Asc = 0,
        Desc = 1
    }
}
